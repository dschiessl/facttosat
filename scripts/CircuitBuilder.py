import math
from enum import Enum, auto

from scripts.Circuit import AndGate, OrGate, NotGate, Circuit


class MultiplicationBase(Enum):
    SCHOOL = auto()
    WALLACE = auto()
    DADDA = auto()


# the number of bits the smaller summand must exceed for the Karatsuba algorithm to be used. Currently, this must be at
# least 3 to ensure termination
DEFAULT_KARATSUBA_THRESHOLD: int = 29

# the multiplication method used when not passing the Karatsuba threshold
DEFAULT_MULTIPLICATION_BASE: MultiplicationBase = MultiplicationBase.DADDA


def add_xor(circuit: Circuit, input1: int, input2: int) -> int:
    """
    Adds a 2-input XOR to the circuit. Currently implemented by AND-ing an OR and a NAND

    :param circuit: the circuit to extend
    :param input1: the index of the first input
    :param input2: the index of the second input
    :return: the index of the output
    """
    or_output: int = circuit.add_gate(OrGate(input1, input2))
    and_output: int = circuit.add_gate(AndGate(input1, input2))
    nand_output: int = circuit.add_gate(NotGate(and_output))
    xor_output: int = circuit.add_gate(AndGate(or_output, nand_output))

    return xor_output


def add_half_adder(circuit: Circuit, input1: int, input2: int) -> (int, int):
    """
    Adds a half-adder to the circuit.

    :param circuit: the circuit to extend
    :param input1: the index of the first input
    :param input2: the index of the second input
    :return: the index of the sum, then the index of the carry
    """
    sum_output: int = add_xor(circuit, input1, input2)
    carry_output: int = circuit.add_gate(AndGate(input1, input2))

    return sum_output, carry_output


def add_full_adder(circuit: Circuit, input1: int, input2: int, carry_over: int) -> (int, int):
    """
    Adds a full-adder to the circuit. Currently implemented using two successive half-adders

    :param circuit: the circuit to extend
    :param input1: the index of the first input
    :param input2: the index of the second input
    :param carry_over: index of the carry from the previous bit
    :return: the index of the sum, then the index of the carry
    """
    # type hints aren't allowed during tuple unpacking, so they're here
    first_sum: int
    first_carry: int
    final_sum: int
    second_carry: int

    first_sum, first_carry = add_half_adder(circuit, input1, input2)
    final_sum, second_carry = add_half_adder(circuit, first_sum, carry_over)
    final_carry: int = circuit.add_gate(OrGate(first_carry, second_carry))

    return final_sum, final_carry


def add_half_adder_with_fixed_one(circuit: Circuit, input_bit: int) -> (int, int):
    """
    Adds a half-adder with one of the inputs set to a 1 to the circuit.

    :param circuit: the circuit to extend
    :param input_bit: the index of the input bit
    :return: the index of the sum, then the index of the carry
    """

    # an XOR with one input set to a 1 is a NOT
    sum_bit: int = circuit.add_gate(NotGate(input_bit))

    # an AND with one bit set to a one just returns the second bit
    return sum_bit, input_bit


def add_full_adder_with_fixed_one(circuit: Circuit, input1: int, input2: int) -> (int, int):
    """
    Adds a full-adder with one of the inputs set to a 1 to the circuit.

    :param circuit: the circuit to extend
    :param input1: the index of the first input bit
    :param input2: the index of the second input bit
    :return: the index of the sum, then the index of the carry
    """

    # the XOR with the two actual inputs is negated by the one with one input set to 1
    sum_bit: int = circuit.add_gate(NotGate(add_xor(circuit, input1, input2)))

    # since one input is already 1, one more suffices for a carry
    carry_bit: int = circuit.add_gate(OrGate(input1, input2))

    # an AND with one bit set to a one just returns the second bit
    return sum_bit, carry_bit


def add_full_adder_chain(circuit: Circuit, first_summand_bits: list[int], second_summand_bits: list[int],
                         carry_over: int) -> list[int]:
    """
    Adds two binary numbers using a chain of full-adders, therefore requiring an additional carry bit to be put in.

    :param circuit: the circuit to extend
    :param first_summand_bits: the indices of the first summand's bits, in ascending order of significance. Must have
    the same length as second_carry_bits.
    :param second_summand_bits: the indices of the second summnand'S bits, in ascending order of significance. Must have
    the same length as first_carry_bits
    :param carry_over: the index of the carry from the previous bit
    :return: the indices of the bits of the sum, in ascending order of significance. Includes the final carry bit.
    """
    if len(first_summand_bits) != len(second_summand_bits):
        raise Exception("The numbers must have the same length")

    sum_bits: list[int] = list[int]()
    last_carry_bit: int = carry_over

    # chaining the full-adders, inputting corresponding bits of the summands and the carry from the previous bit
    for i in range(len(first_summand_bits)):
        next_sum_bit: int
        next_carry_bit: int
        next_sum_bit, next_carry_bit = add_full_adder(circuit, first_summand_bits[i], second_summand_bits[i],
                                                      last_carry_bit)
        sum_bits.append(next_sum_bit)
        last_carry_bit = next_carry_bit

    # if there was a carry on the last bit, it belongs to the sum as well
    sum_bits.append(last_carry_bit)

    return sum_bits


def add_addition(circuit: Circuit, first_summand_bits: list[int], second_summand_bits: list[int], shift: int = 0) -> \
        list[int]:
    """
    Adds a circuit which adds two binary numbers, possibly with a shift, though there must not be a gap between them.

    :param circuit: the circuit to extend
    :param first_summand_bits: the indices of the first summand's bits, in ascending order of significance.
    :param second_summand_bits: the indices of the second summnand's bits, in ascending order of significance.
    :param shift: the amount of bits by which the second summand is to be shifted to the left. Must not be negative.
    :return: the indices of the bits of the sum, in ascending order of significance
    """

    if shift < 0:
        raise Exception("Negative shifts are not supported")

    num_of_overlapping_bits: int = min(len(first_summand_bits) - shift, len(second_summand_bits))

    if num_of_overlapping_bits < 0:
        raise Exception("Unsupported input dimensions")
    elif num_of_overlapping_bits == 0:
        # since there is neither an overlap nor a gap, we can simply concatenate the inputs
        return first_summand_bits.copy() + second_summand_bits.copy()

    # the bits before the second summand exists
    right_overhang: list[int] = first_summand_bits[:shift]

    # the bits where both summands exist
    first_summand_overlap: list[int] = first_summand_bits[shift:shift + num_of_overlapping_bits]
    second_summand_overlap: list[int] = second_summand_bits[:num_of_overlapping_bits]

    # the bits after one summand ended
    left_overhang: list[int]
    if len(first_summand_bits) - shift >= len(second_summand_bits):
        # the first summand overhangs (or there is no overhang at all)
        left_overhang = first_summand_bits[shift + num_of_overlapping_bits:]
    else:
        # the second summand overhangs
        left_overhang = second_summand_bits[num_of_overlapping_bits:]

    # for the first bit of the overlap (note that there must be at least one), a half-adder is used, for the rest a
    # chain of full-adders
    first_sum_bit_of_overlap: int
    first_carry_bit_of_overlap: int
    first_sum_bit_of_overlap, first_carry_bit_of_overlap = add_half_adder(circuit, first_summand_overlap[0],
                                                                          second_summand_overlap[0])
    other_sum_bits_of_overlap: list[int] = add_full_adder_chain(circuit, first_summand_overlap[1:],
                                                                second_summand_overlap[1:], first_carry_bit_of_overlap)

    # the outgoing carry of the overlap is propagated through the left overhang using half-adders
    # isolating the carry bit
    carry_bit: int = other_sum_bits_of_overlap[-1]
    other_sum_bits_of_overlap: list[int] = other_sum_bits_of_overlap[:-1]

    # propagating
    for i in range(len(left_overhang)):
        left_overhang[i], carry_bit = add_half_adder(circuit, left_overhang[i], carry_bit)

    return right_overhang + [first_sum_bit_of_overlap] + other_sum_bits_of_overlap + left_overhang + [carry_bit]


def add_subtraction(circuit: Circuit, minuend_bits: list[int], subtrahend_bits: list[int]) -> list[int]:
    """
    Adds a circuit which adds two binary numbers.

    :param circuit: the circuit to extend
    :param minuend_bits: the indices of the minuend's bits, in ascending order of significance.
    :param subtrahend_bits: the indices of the subtrahend's bits, in ascending order of significance.
    :return: the indices of the bits of the difference, in ascending order of significance
    """

    if len(subtrahend_bits) == 0:
        # if nothing is subtracted, the minuend stays unchanged
        return minuend_bits.copy()
    else:
        # the subtrahend is turned to the 1-complement here, the +1 for the 2-complement is integrated into the addition
        subtrahend_bits = list(map(lambda bit: circuit.add_gate(NotGate(bit)), subtrahend_bits))

    current_carry: int

    # the first bit
    first_difference_bit: int
    if len(minuend_bits) == 0:
        # since there is no minuend, we only need to add the 1 for the 2-complement to the subtrahend
        first_difference_bit, current_carry = add_half_adder_with_fixed_one(circuit, subtrahend_bits[0])
    else:
        # since there is a minuend, we need to add the minuend, the subtrahend, and the 1 for the 2-complement
        first_difference_bit, current_carry = add_full_adder_with_fixed_one(circuit, minuend_bits[0],
                                                                            subtrahend_bits[0])

    # the remaining bits where both inputs exist. Set to 1 even if there is no overlap since the first bit has already
    # been handled and this size is also used as the start of the overhang
    overlap_size: int = max(min(len(minuend_bits), len(subtrahend_bits)), 1)
    overlap_difference: list[int] = add_full_adder_chain(circuit, minuend_bits[1:overlap_size],
                                                         subtrahend_bits[1:overlap_size], current_carry)

    # separating the carry_bit
    current_carry = overlap_difference[-1]
    overlap_difference = overlap_difference[:-1]

    # the bits where only one input exists. The for loop for the other input will be skipped
    overhang_difference: list[int] = list()
    current_difference_bit: int
    for i in range(overlap_size, len(minuend_bits)):
        # in addition to propagating the carry, we need to keep adding 1s for the padding of the subtrahend.
        current_difference_bit, current_carry = add_full_adder_with_fixed_one(circuit, minuend_bits[i], current_carry)
        overhang_difference.append(current_difference_bit)
    for i in range(overlap_size, len(subtrahend_bits)):
        # only the carry needs to be propagated
        current_difference_bit, current_carry = add_half_adder(circuit, subtrahend_bits[i], current_carry)
        overhang_difference.append(current_difference_bit)

    # adding the 1 from the subtrahend's padding to the last carry bit, ignoring any overflow
    final_carry_bit: int = circuit.add_gate(NotGate(current_carry))

    return [first_difference_bit] + overlap_difference + overhang_difference + [final_carry_bit]


def add_one_bit_multiplication(circuit: Circuit, bits_to_multiply: list[int], bit_to_multiply_by: int) -> list[int]:
    """
    Multiplies (ANDs) a binary number by a single bit.

    :param circuit: the circuit ot extend
    :param bits_to_multiply: the indices of the bits of the number ot multiply
    :param bit_to_multiply_by: the index of the bit to multiply the number by
    :return: the indices of the bits of the product
    """
    return list(map(lambda bit: circuit.add_gate(AndGate(bit, bit_to_multiply_by)), bits_to_multiply))


def add_multiplication(circuit: Circuit, first_factor_bits: list[int], second_factor_bits: list[int],
                       karatsuba_threshold: int = None, multiplication_base: MultiplicationBase = None) -> list[int]:
    """
    Multiplies two binary numbers

    :param circuit: the circuit to extend
    :param first_factor_bits: the indices of the bits of the first factor, in ascending order of significance
    :param second_factor_bits: the indices of the bits of the second factor, in ascending order of significance.
    :param karatsuba_threshold: the size the smaller factor must exceed for the Karatsuba algorithm to be used. Must be
    at least 3 to ensure termination
    :param multiplication_base: the multiplication method used if the Karatsuba threshold is not passed
    :return: the indices of the bits of the product, in ascending order of significance
    """
    # default values
    if karatsuba_threshold is None:
        karatsuba_threshold = DEFAULT_KARATSUBA_THRESHOLD
    if multiplication_base is None:
        multiplication_base = DEFAULT_MULTIPLICATION_BASE

    smaller_input_size: int = min(len(first_factor_bits), len(second_factor_bits))
    if smaller_input_size < 1:
        # at least one of the inputs is empty, so the result is always 0:
        return []
    elif smaller_input_size == 1:
        # one bit multiplication
        if len(second_factor_bits) == 1:
            # the second factor is the single bit
            return add_one_bit_multiplication(circuit, first_factor_bits, second_factor_bits[0])
        else:
            # the first factor is the single bit
            return add_one_bit_multiplication(circuit, second_factor_bits, first_factor_bits[0])
    elif smaller_input_size > karatsuba_threshold:
        # the Karatsuba algorithm can be used
        return add_karatsuba(circuit, first_factor_bits, second_factor_bits, karatsuba_threshold, multiplication_base)
    else:
        summands: list[list[int]] = list(
            map(lambda bit: add_one_bit_multiplication(circuit, first_factor_bits, bit), second_factor_bits))

        if multiplication_base == MultiplicationBase.SCHOOL:
            # "school method"
            current_sum: list[int] = summands[0]
            for i in range(1, len(summands)):
                current_sum = add_addition(circuit, current_sum, summands[i], i)
            return current_sum
        else:
            # reduction using a multiplication tree
            columns: list[list[int]] = summands_to_columns(summands)

            reduced_columns: list[list[int]]
            if multiplication_base == MultiplicationBase.DADDA:
                reduced_columns = add_dadda_tree(circuit, columns)
            elif multiplication_base == MultiplicationBase.WALLACE:
                reduced_columns = add_wallace_tree(circuit, columns)
            else:
                raise ValueError("Multiplication method not supported")

            return add_reduced_columns_addition(circuit, reduced_columns)



def summands_to_columns(summands: list[list[int]]) -> list[list[int]]:
    """
    Converts a list of summands, each shifted by one bit more than the previous one, into a list of bit columns

    :param summands: the indices of the bits of the summands, starting with the rightmost summand. The bits within the
    summands must be in ascending order of magnitude, each summand must have the same size and there must be at least
    two summands and two bits per summand
    :return: a list of columns of the indices of the bits, in ascending order of significance
    """
    num_of_summands: int = len(summands)
    size_of_summands: int = len(summands[0])

    # there are (num_of_summands + size_of_summands - 1) columns because each summand is shifted by one bit more
    columns: list[list[int]] = list()
    for i in range(num_of_summands + size_of_summands - 1):
        columns.append(list())

    for i in range(num_of_summands):
        for j in range(size_of_summands):
            columns[i + j].append(summands[i][j])

    return columns


def add_reduced_columns_addition(circuit: Circuit, columns: list[list[int]]) -> list[int]:
    """
    Takes bit columns which have been reduced by a multiplication tree and adds their numbers

    :param circuit: the circuit to extend
    :param columns: the columns of bit indices, in ascending order of significance. Each column must have one or two
    bits and there must be no gap between the columns with two bits
    :return: the indices of the bits of the sum, in ascending order of significance
    """
    first_summand: list[int] = list()
    second_summand: list[int] = list()
    shift: int = 0

    # calculating the shift
    while len(columns[shift]) < 2:
        shift += 1

    # collecting the bits of the summands
    for column in columns:
        first_summand.append(column[0])

        if len(column) > 1:
            second_summand.append(column[1])

    # adding
    return add_addition(circuit, first_summand, second_summand, shift)


def add_wallace_tree(circuit: Circuit, columns: list[list[int]]) -> list[list[int]]:
    """
    Reduces bit columns using a Wallace tree

    :param circuit: the circuit to extend
    :param columns: the columns of bit indices, in ascending order of significance, as returned by summands_to_columns.
    Must contain at least one column with at least two bits
    :return: the reduced columns of bit indices, in ascending order of significance.
    """
    while True:
        max_column_height = max(map(len, columns))
        if max_column_height <= 2:
            break

        reduced_columns: list[list[int]] = list()
        for i in range(len(columns)):
            reduced_columns.append(list())
        if len(columns[-1]) > 1:
            # there will be a carry
            reduced_columns.append(list())

        # reduction layer
        for i in range(len(columns)):
            if len(columns[i]) < 2:
                # the entire column is simply passed on
                reduced_columns[i] += columns[i]
            elif len(columns[i]) == 2:
                # reduced by a half adder
                sum, carry = add_half_adder(circuit, *columns[i])
                reduced_columns[i].append(sum)
                reduced_columns[i + 1].append(carry)
            else:
                # reduced by a full adder
                sum, carry = add_full_adder(circuit, *columns[i][-3:])
                reduced_columns[i].append(sum)
                reduced_columns[i] += columns[i][:-3]
                reduced_columns[i + 1].append(carry)

        columns = reduced_columns

    return columns


def add_dadda_tree(circuit: Circuit, columns: list[list[int]]) -> list[list[int]]:
    """
    Reduces bit columns using a Dadda tree

    :param circuit: the circuit to extend
    :param columns: the columns of bit indices, in ascending order of significance, as returned by summands_to_columns.
    Must contain at least one column with at least two bits
    :return: the reduced columns of bit indices, in ascending order of significance.
    """
    max_column_height = max(map(len, columns))
    height_limit_sequence: list[int] = [2]
    while math.floor(1.5 * height_limit_sequence[-1]) < max_column_height:
        height_limit_sequence.append(math.floor(1.5 * height_limit_sequence[-1]))

    # reduction layer
    for height_limit in reversed(height_limit_sequence):
        reduced_columns: list[list[int]] = list()
        for i in range(len(columns)):
            reduced_columns.append(list())
        if len(columns[-1]) > height_limit:
            # there will be a carry
            reduced_columns.append(list())

        for i in range(len(columns)):
            num_of_bits_in_adders: int = 0

            while True:
                num_of_bits_to_reduce_by: int = len(columns[i]) - num_of_bits_in_adders + len(
                    reduced_columns[i]) - height_limit
                if num_of_bits_to_reduce_by < 1:
                    # the reduction of this column is finished. The bits from the adders are added last so that they
                    # are the last ones considered for the next adders
                    reduced_columns[i] = columns[i][num_of_bits_in_adders:] + reduced_columns[i]
                    break
                elif num_of_bits_to_reduce_by == 1:
                    # reduction using a half adder
                    sum, carry = add_half_adder(circuit, *columns[i][num_of_bits_in_adders:num_of_bits_in_adders + 2])
                    reduced_columns[i].append(sum)
                    reduced_columns[i + 1].append(carry)
                    num_of_bits_in_adders += 2
                else:
                    # reduction using a full adder
                    sum, carry = add_full_adder(circuit, *columns[i][num_of_bits_in_adders:num_of_bits_in_adders + 3])
                    reduced_columns[i].append(sum)
                    reduced_columns[i + 1].append(carry)
                    num_of_bits_in_adders += 3

        columns = reduced_columns

    return columns


def add_karatsuba(circuit: Circuit, first_factor_bits: list[int], second_factor_bits: list[int],
                  karatsuba_threshold: int, multiplication_base: MultiplicationBase) -> list[int]:
    """
    Multiplies two binary numbers using the Karatsuba algorithm

    :param circuit: the circuit to extend
    :param first_factor_bits: the indices of the bits of the first factor, in ascending order of significance
    :param second_factor_bits: the indices of the bits of the second factor, in ascending order of significance.
    :param karatsuba_threshold: the size the smaller factor must exceed for the Karatsuba algorithm to be used. Must be
    at least 3 to ensure termination. Passed on to the sub-multiplications.
    :param multiplication_base: the multiplication method used if the Karatsuba threshold is not passed. Passed on to
    the sub-multiplications.
    :return: the indices of the bits of the product, in ascending order of significance
    """
    # obviously, we try to drop the maximum below the threshold. It would be utterly nonsensical to use the minimum here
    input_size: int = max(len(first_factor_bits), len(second_factor_bits))

    # it is arbitrary whether to round down or up
    shift: int = math.ceil(input_size / 2)

    lower_product: list[int] = add_multiplication(circuit, first_factor_bits[:shift], second_factor_bits[:shift],
                                                  karatsuba_threshold, multiplication_base)
    upper_product: list[int] = add_multiplication(circuit, first_factor_bits[shift:], second_factor_bits[shift:],
                                                  karatsuba_threshold, multiplication_base)

    # this uses the simple version of the algorithm (without negative numbers)
    middle_product_first_factor: list[int] = add_addition(circuit, first_factor_bits[:shift], first_factor_bits[shift:])
    middle_product_second_factor: list[int] = add_addition(circuit, second_factor_bits[:shift],
                                                           second_factor_bits[shift:])

    middle_product: list[int] = add_multiplication(circuit, middle_product_first_factor, middle_product_second_factor,
                                                   karatsuba_threshold, multiplication_base)

    middle_summand: list[int] = add_subtraction(circuit, middle_product, lower_product)
    middle_summand = add_subtraction(circuit, middle_summand, upper_product)

    result: list[int] = add_addition(circuit, lower_product, middle_summand, shift)
    result = add_addition(circuit, result, upper_product, shift * 2)

    return result
