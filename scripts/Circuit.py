from collections import Iterator

from scripts.Cnf import BooleanVariable, Literal, Cnf


class Gate:
    def __init__(self, *inputs: int) -> None:
        """
        :param inputs: the indices of the input gates, which must have been added to the Circuit before this one.
        WARNING: some gates may allow only a limited number of inputs
        """
        self.inputs: list[int] = list(inputs)

    def add_to_cnf(self, cnf: Cnf, *input_literals: Literal) -> Literal:
        """
        Transforms this gate to cnf clauses and adds them to the given formula

        :param cnf: the formula to extend
        :param input_literals: the literals containing the input of the gate.
        WARNING: some gates may allow only a limited number of inputs
        :return the Literal containing the output of the Gate
        """
        pass


class InputGate(Gate):
    """
    Placeholder for external inputs. Can't have  any inputs of is own.
    """

    def get_variable(self) -> BooleanVariable:
        """Allows access to the variable used for this input in the last call to add_to_cnf

        :return the variable
        """
        return self.variable

    def add_to_cnf(self, cnf: Cnf, *input_literals: Literal) -> Literal:
        self.variable: BooleanVariable = cnf.create_variable()
        return Literal(self.variable)


class NotGate(Gate):
    """
    Must have exactly one input.
    """

    def add_to_cnf(self, cnf: Cnf, *input_literals: Literal) -> Literal:
        # simply inverts the input Literal
        return input_literals[0].inverted()


class AndGate(Gate):
    """
    Arbitrary number of inputs allowed.
    """

    def add_to_cnf(self, cnf: Cnf, *input_literals: Literal) -> Literal:
        output_literal: Literal = Literal(cnf.create_variable())

        # Tseitin transformation
        for literal in input_literals:
            cnf.add_clause(output_literal.inverted(), literal)

        negated_input_literals: Iterator[Literal] = map(lambda literal: literal.inverted(), input_literals)
        cnf.add_clause(output_literal, *negated_input_literals)

        return output_literal


class OrGate(Gate):
    """
    Arbitrary number of inputs allowed.
    """

    def add_to_cnf(self, cnf: Cnf, *input_literals: Literal) -> Literal:
        output_literal: Literal = Literal(cnf.create_variable())

        # Tseitin transformation
        cnf.add_clause(output_literal.inverted(), *input_literals)

        for literal in input_literals:
            cnf.add_clause(output_literal, literal.inverted())

        return output_literal


class Circuit:
    def __init__(self) -> None:
        self.gates: list[Gate] = list[Gate]()

    def add_gate(self, to_add: Gate) -> int:
        """
        Adds a gate to the Circuit. Will not set its inputs.

        :param to_add: the Gate to add
        :return: the index at which the Gate was added
        """

        self.gates.append(to_add)
        return len(self.gates) - 1

    def get_gate(self, index: int) -> Gate:
        """
        Allows accessing a gate of the circuit

        :param index: the index of the gate
        :return: the gate at the given index
        """
        return self.gates[index]

    def to_cnf(self) -> Cnf:
        """
        Converts this Circuit to a Cnf

        :return: the created Cnf object
        """
        result: Cnf = Cnf()

        # the output literals for each Gate
        literals: list[Literal] = list()

        # add clauses to implement the logic of the Gates
        for gate in self.gates:
            # the map translates the input indices to the variables holding der values. Self-loops will cause an
            # OutOfBoundsException here
            output_literal: Literal = gate.add_to_cnf(result, *map(lambda input: literals[input], gate.inputs))
            # now we can add the new variable to the list
            literals.append(output_literal)

        # add a unit clause to check whether the output of the last Gate is true
        result.add_clause(literals[-1])

        return result
