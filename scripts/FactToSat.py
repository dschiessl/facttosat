import io
import math

from scripts import CircuitBuilder, Util
from scripts.Circuit import Circuit, InputGate, NotGate, AndGate
from scripts.Cnf import Cnf, BooleanVariable

FACTORIZED_NUMBER_MARKER: str = "n "
FIRST_FACTOR_BITS_MARKER: str = "f1 "
SECOND_FACTOR_BITS_MARKER: str = "f2 "

FORMULA_COMMENT_HEAD: str = f"CNF encoding of the factorization problem.\n" \
                            f"The following lines contain the number to factor (prepended by an " \
                            f"\"{FACTORIZED_NUMBER_MARKER}\") and the variables encoding the bits\nof the factors " \
                            f"(prepended by \"{FIRST_FACTOR_BITS_MARKER}\" and \"{SECOND_FACTOR_BITS_MARKER}\"; in " \
                            f"each case in ascending order of significance)\n\n"


def fact_to_sat(to_factor: int, karatsuba_threshold: int = None,
                multiplication_base: CircuitBuilder.MultiplicationBase = None) -> Cnf:
    """
    Creates a cnf representing the factorization problem for the given number

    :param to_factor: the number which should be factored
    :param karatsuba_threshold: the size the smaller factor must exceed for the Karatsuba algorithm to be used. Must be
    at least 3 to ensure termination
    :param multiplication_base: the multiplication method used if the Karatsuba threshold is not passed
    :return: the created cnf
    """
    # variables shared between both cases of the following if
    formula: Cnf
    smaller_factor_variables: list[BooleanVariable]
    larger_factor_variables: list[BooleanVariable]

    if to_factor < 4:
        # the current conversion always considers numbers with at most 2 bits composite since the product has too few
        # bits. Luckily, the lowest composite number (4) has 3 bits, so we can simply return an unsatisfiable formula
        #  for smaller ones
        formula = Cnf()

        # the empty clause
        formula.add_clause()

        # there are no variables
        smaller_factor_variables = list()
        larger_factor_variables = list()

    else:
        # will generate a circuit which takes two binary numbers, multiplies them and check if the result matches the
        # number to factor. This circuit is then transformed to a cnf

        to_factor_bits = Util.integer_to_binary_string(to_factor)

        # the smaller factor is at most the square root of the original number
        smaller_factor_bit_number = math.ceil(len(to_factor_bits) / 2)
        # the larger factor can't be more than half the original number if the smaller one is at least 2
        larger_factor_bit_number = len(to_factor_bits) - 1

        circuit: Circuit = Circuit()

        # the inputs
        smaller_factor_bits: list[int] = list(
            map(lambda i: circuit.add_gate(InputGate()), range(smaller_factor_bit_number)))
        larger_factor_bits: list[int] = list(
            map(lambda i: circuit.add_gate(InputGate()), range(larger_factor_bit_number)))

        # the multiplication
        product_bits: list[int] = CircuitBuilder.add_multiplication(circuit, larger_factor_bits, smaller_factor_bits,
                                                                    karatsuba_threshold, multiplication_base)

        # to compare to to_factor, we add NOT gates to every bit which must be false, thereby creating a string for which
        # each bit must be true. This can now simply be checked with an AND
        for i in range(len(product_bits)):
            if len(to_factor_bits) <= i or not to_factor_bits[i]:
                product_bits[i] = circuit.add_gate(NotGate(product_bits[i]))

        circuit.add_gate(AndGate(*product_bits))

        # converting
        formula = circuit.to_cnf()

        # getting variables for comment
        smaller_factor_variables = list(map(lambda index: circuit.get_gate(index).get_variable(),
                                                                   smaller_factor_bits))
        larger_factor_variables = list(map(lambda index: circuit.get_gate(index).get_variable(),
                                                                  larger_factor_bits))
    # adding the comment
    comment: str = FORMULA_COMMENT_HEAD
    comment += FACTORIZED_NUMBER_MARKER + str(to_factor) + "\n"
    comment += FIRST_FACTOR_BITS_MARKER + " ".join(["{}"] * len(smaller_factor_variables)) + "\n"
    comment += SECOND_FACTOR_BITS_MARKER + " ".join(["{}"] * len(larger_factor_variables)) + "\n"
    formula.set_comment(comment, smaller_factor_variables + larger_factor_variables)

    return formula


def get_factor_variables(dimacs: io.TextIOBase) -> (list[int], list[int]):
    """
    Extracts which variables encode the bits of the factor from the comment set by fact_to_sat

    :param dimacs: a stream containing the DIMACS-encoded formula
    :return: the numbers of the variables encoding the  bits of the two factors, each in ascending order of significance
    """
    first_factor: list[int] = list()
    second_factor: list[int] = list()

    # helper function which extracts the ints from the line with a factor
    def string_to_int_list(string: str) -> list[int]:
        return list(map(int, string.split(" ")[1:]))

    while True:
        # if we run out of lines, the file was incorrectly formatted
        line: str = dimacs.readline()

        if line.startswith("c "):
            # we are in the comments, the "c " can now be cut away
            line = line[2:]
            if line.startswith(FIRST_FACTOR_BITS_MARKER):
                first_factor = string_to_int_list(line)
            elif line.startswith(SECOND_FACTOR_BITS_MARKER):
                second_factor = string_to_int_list(line)
            if first_factor and second_factor:
                # we are done, no need to read the whole formula
                break
        else:
            # we have passed the comments without finding the markings
            break

    return first_factor, second_factor
