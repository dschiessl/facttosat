import io
from collections import Iterator


class BooleanVariable:
    """Instances of this class are used as ids for variables, taking advantage of the system's memory management.
    To ensure different instances are always recognized as different, __eq__ must not be implemented"""
    pass


class Literal:
    def __init__(self, variable: BooleanVariable, polarity: bool = True) -> None:
        """
        :param variable: id of a variable created by a Cnf's create_variable method
        :param polarity: True for a positive literal, False for a negative one
        """
        self.variable: BooleanVariable = variable
        self.polarity: bool = polarity

    def __hash__(self) -> int:
        return hash(self.variable) ^ hash(self.polarity)

    def __eq__(self, other) -> bool:
        return self.variable == other.variable and self.polarity == other.polarity

    def inverted(self):
        """
        Creates an inverted version of itself

        :return: the created literal
        """

        return Literal(self.variable, not self.polarity)


class Cnf:
    def __init__(self) -> None:
        self.registered_variables: list[BooleanVariable] = list()
        self.clauses: set[frozenset[Literal]] = set()
        self.comment: str = ""
        self.comment_variables: list[BooleanVariable] = list()

    def create_variable(self) -> BooleanVariable:
        """
        Registers a new boolean variable for use in future clauses.
        When converted to DIMACS, the variables will be numbered according to the order in which they where reserved.

        :return: the id of the new variable
        """
        new_variable: BooleanVariable = BooleanVariable()
        self.registered_variables.append(new_variable)
        return new_variable

    def add_clause(self, *literals: Literal) -> None:
        clause: frozenset[Literal] = frozenset(literals)

        # checking if the clause is a tautology
        variables: set[BooleanVariable] = set()
        for literal in clause:
            if literal.variable not in variables:
                variables.add(literal.variable)
            else:
                # since duplicate literals have already been removed, the duplicate variable means contradictory
                # literals and therefore a tautological clause, so there is nothing to add
                return

        self.clauses.add(clause)

    def set_comment(self, comment: str, comment_variables: list[BooleanVariable] = list()) -> None:
        """
        Sets the comment for the formula.

        :param comment: the comment. Can contain "{}"s, which will be replaced with the names of the variables in
        comment_variables
        :param comment_variables: the variables whose names should be inserted into the comment
        """
        self.comment = comment
        self.comment_variables = comment_variables

    def to_dimacs(self, stream: io.TextIOBase) -> None:
        """
        Converts this formula to the DIMACS format and writes it to the given stream

        :param stream: the stream to which the result should be written
        """
        # mark actually used variables
        for clause in self.clauses:
            for literal in clause:
                literal.variable.is_used: bool = True

        # add used variables to a new list, keeping their original order and numbering them according to it
        used_variables: list[BooleanVariable] = list()
        for variable in self.registered_variables:
            if hasattr(variable, "is_used"):
                del variable.is_used
                used_variables.append(variable)
                variable.number: int = len(used_variables)

        # comments
        if self.comment:
            # maps the comment_variables to their number, or to 0 if they have none
            comment_variable_names: Iterator[int] = map(lambda var: var.number if hasattr(var, "number") else 0,
                                                        self.comment_variables)

            # puts the variable numbers in the comment
            formatted_comment: str = self.comment.format(*comment_variable_names)
            # adds the "c "s
            comment_lines: list[str] = formatted_comment.split("\n")
            final_comment: str = "\n".join(map(lambda line: "c " + line, comment_lines))

            stream.write(final_comment + "\n")

        # header
        num_of_variables: int = len(used_variables)
        num_of_clauses: int = len(self.clauses)
        stream.write(f"p cnf {num_of_variables} {num_of_clauses}\n")

        # clauses

        def literal_to_str(literal: Literal) -> str:
            # the variable's number, prepended by a "-" if negated
            return ("" if literal.polarity else "-") + str(literal.variable.number)

        def clause_to_str(clause: Iterator[Literal]) -> str:
            # converts Literals to strings, adds 0 at the end, connects them with spaces.
            clause_strings: list[str] = list(map(literal_to_str, clause))
            clause_strings.append("0")
            return " ".join(clause_strings) + "\n"

        # converts every clause to string, then writes it to the stream
        stream.writelines(list(map(clause_to_str, self.clauses)))

        # cleanup from previous numbering
        for variable in used_variables:
            del variable.number
