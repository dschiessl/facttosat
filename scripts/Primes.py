import math

from scripts import Util
import random

# the error probability of the MRT is at most (1/4)**iterations
MRT_ITERATIONS = 50


def mrt(number: int) -> bool:
    """
    Implementation of the Miller-Rabin test

    :param number: the number to test. Must be odd.
    :return: a boolean stating whether the number is prime
    """

    exponent_bits = Util.integer_to_binary_string(number - 1)

    for i in range(MRT_ITERATIONS):
        base: int = random.randint(1, number - 1)
        power: int = 1
        previous_power: int = 1

        # square-and-multiply with check for illegal roots of 1
        for bit in reversed(exponent_bits):
            # square
            previous_power = power
            power = power ** 2 % number

            # the only roots of 1 are 1 and -1
            if power == 1 and previous_power != 1 and previous_power != number - 1:
                return False

            # multiply
            if bit:
                power = power * base % number

        # Fermat test
        if power != 1:
            return False

    return True


def test_if_prime(number: int) -> bool:
    """
    Tests if an arbitrary number is prime

    :param number: the number to test
    :return: a boolean stating whether the number is prime
    """
    if number < 2:
        # there are no primes below 2
        return False
    elif number == 2:
        # even numbers are filtered out afterwards, but 2 is prime
        return True
    elif number % 2 == 0:
        # if it is not 2, a even number can not be prime
        return False
    else:
        # now, the MRT can be applied
        return mrt(number)


def gcd(first: int, second: int) -> int:
    """
    Implementation of Euler's algorithm for the greatest common divisor

    :param first: the first number
    :param second: the second number
    :return: the greatest common divisor of the numbers
    """
    if second == 0:
        # break condition
        return first
    else:
        # recursive call
        return gcd(second, first % second)


def rho_factorize(to_factorize: int, recursive: bool = True) -> list[int]:
    """
    Implementation of Pollard's Rho-algorithm for factorization

    :param to_factorize: the number to factorize
    :param recursive: if true, the function will call itself recursively to find the factor's factors. Otherwise it
    stops after finding one factor
    :return: a list of the number's prime factors
    """

    # a constant pseudo random generator could possibly only generate unlucky cycles, so we add some variation
    summand_in_pseudo_random_generator: int = random.randint(0, to_factorize - 1)

    # generates the next value
    def pseudo_random_generator(prev: int):
        return (prev ** 2 + summand_in_pseudo_random_generator) % to_factorize

    if test_if_prime(to_factorize):
        # break condition
        return [to_factorize]
    if to_factorize % 2 == 0:
        # multiples of 2 can cause problems
        if recursive:
            # factorizes the factors
            return [2] + rho_factorize(to_factorize // 2, recursive)
        else:
            # just returns the first factor pair
            return [2, to_factorize // 2]
    else:
        # the two numbers moving through the rho to detect the cycle
        slower: int = random.randint(0, to_factorize - 1)
        faster: int = slower

        potential_divisor: int = 1
        while potential_divisor == 1:
            slower = pseudo_random_generator(slower)
            faster = pseudo_random_generator(pseudo_random_generator(faster))
            potential_divisor = gcd(abs(slower - faster), to_factorize)

        if potential_divisor == to_factorize:
            # we were unlucky, retry
            return rho_factorize(to_factorize, recursive)
        else:
            # divisor found
            if recursive:
                # recursive call to find sub-divisors
                return rho_factorize(potential_divisor, recursive) + rho_factorize(to_factorize // potential_divisor, recursive)
            else:
                # just returns the found divisor and its opposite
                return [potential_divisor, to_factorize // potential_divisor]


def naive_factorize(to_factorize: int, recursive: bool = True) -> list[int]:
    """
    Implementation of the naive factorization algorithm

    :param to_factorize: the number to factorize
    :param recursive: if true, the function will call itself recursively to find the factor's factors. Otherwise it
    stops after finding one factor
    :return: a list of the number's prime factors
    """

    # if there are true divisors, they can't all be lower than the square root
    upper_bound: int = math.isqrt(to_factorize)

    for potential_divisor in range(2, upper_bound + 1):
        if to_factorize % potential_divisor == 0:
            # divisor found
            if recursive:
                # recursive call to find sub-divisors
                return [potential_divisor] + naive_factorize(to_factorize // potential_divisor, recursive)
            else:
                # just returns the found divisor and its opposite
                return [potential_divisor, to_factorize // potential_divisor]

    # we got through every potential divisor, but none was an actual divisor, so the number is prime
    return [to_factorize]


def get_prime(size: int) -> int:
    """
    Generates a random prime number

    :param size: the size in bits of the prime to generate
    :return: the generated prime
    """
    if size < 2:
        # there are no primes with less than two bytes
        raise Exception("Invalid size")
    elif size == 2:
        # since we are only considering odd numbers, the 2 needs a special case
        return random.randint(2, 3)

    # repeated until a prime is found
    while True:
        # a random odd number with exactly size bytes
        candidate: int = (1 << (size - 1)) + (random.getrandbits(size - 2) << 1) + 1

        # testing primality
        if mrt(candidate):
            return candidate
