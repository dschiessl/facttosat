from collections import Iterator


def was_sat(solver_output: str) -> bool:
    """
    Will parse the output of a SAT solver to determine if the formula was satisfiable.

    :param solver_output: the output of the SAT solver
    :return: a boolean stating whether the formula was satisfiable
    """

    lines: list[str] = solver_output.split("\n")

    for line in lines:
        if line.startswith("s"):
            if line == "s SATISFIABLE":
                return True
            else:
                return False


def get_satisfying_assignment(solver_output: str) -> dict[bool]:
    """
    Will parse the output of a SAT solver to determine the satisfying assignment. Assumes the formula is satisfiable

    :param solver_output: the output of the SAT solver
    :return: a dictionary with the values of the variables.
    """

    result = dict()

    lines: list[str] = solver_output.split("\n")

    for line in lines:
        if line.startswith("v"):
            # isolating the variables
            variables: Iterator[int] = map(int, line.split(" ")[1:])

            # translating
            for variable in variables:
                # 0 is intentionally skipped
                if variable > 0:
                    result[variable] = True
                elif variable < 0:
                    result[-variable] = False

    return result
