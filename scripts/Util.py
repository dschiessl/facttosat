from collections import Reversible


def integer_to_binary_string(to_convert: int) -> list[bool]:
    """
    Converts an integer toa string of booleans indicating the bits of the binary representation

    :param to_convert: the number to convert
    :return: the bits of the binary representation of to_convert, in ascending order of significance
    """
    result: list[bool] = list()
    while to_convert:
        # appending the last bit to the result, then removing it from the input
        result.append(bool(to_convert & 1))
        to_convert >>= 1

    return result


def binary_string_to_integer(to_convert: list[bool]) -> int:
    """
    Converts a string of booleans indicating the bits of the binary representation to an integer

    :param to_convert: the bitstring to convert, in ascending order of significance
    :return: the integer representation of to_convert
    """

    result: int = 0

    # double-and-add
    for bit in reversed(to_convert):
        result <<= 1
        result += bit

    return result
