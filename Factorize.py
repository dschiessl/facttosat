import argparse
import os
import subprocess
import tempfile
from collections import Counter

from scripts import FactToSat, SolverHandler, Util, Primes

# os.path.dirname(__file__) returns absolute path to the directory of this script
DEFAULT_SOLVER: str = os.path.dirname(__file__) + "/solvers/kissat"


def sat_factorize(to_factorize: int, recursive: bool = True, solver: str = DEFAULT_SOLVER,
                  karatsuba_threshold: int = None,
                  multiplication_base: FactToSat.CircuitBuilder.MultiplicationBase = None) -> list[int]:
    """
    Finds a number's prime factors

    :param to_factorize: the number to factorize
    :param recursive: if true, the function will call itself recursively to find the factor's factors. Otherwise it
    stops after finding one factor
    :param solver: the SAT solver to use
    :param karatsuba_threshold: the size the smaller factor must exceed for the Karatsuba algorithm to be used. Must be
    at least 3 to ensure termination
    :param multiplication_base: the multiplication method used if the Karatsuba threshold is not passed
    :return: a list of the number's prime factors
    """
    # generating formula
    with tempfile.TemporaryFile(mode="w+") as formula:
        FactToSat.fact_to_sat(to_factorize).to_dimacs(formula)

        # calling solver
        formula.seek(0)
        solver_result: subprocess.CompletedProcess = subprocess.run([solver], stdin=formula, capture_output=True,
                                                                    text=True)

        if not SolverHandler.was_sat(solver_result.stdout):
            # the number was prime
            return [to_factorize]
        else:
            # locating the bits of the factors
            first_factor_bits: list[int]
            second_factor_bits: list[int]
            formula.seek(0)
            first_factor_bits, second_factor_bits = FactToSat.get_factor_variables(formula)

            # extracting the factors
            variable_values = SolverHandler.get_satisfying_assignment(solver_result.stdout)
            first_factor: int = Util.binary_string_to_integer(
                list(map(lambda var: variable_values[var], first_factor_bits)))
            second_factor: int = Util.binary_string_to_integer(
                list(map(lambda var: variable_values[var], second_factor_bits)))

            if recursive:
                # factorizing the factors
                return sat_factorize(first_factor, recursive, solver, karatsuba_threshold,
                                     multiplication_base) + sat_factorize(second_factor, recursive, solver,
                                                                          karatsuba_threshold, multiplication_base)
            else:
                # just return the factor pair
                return [first_factor, second_factor]


if __name__ == '__main__':
    # setting up parser
    parser: argparse.ArgumentParser = argparse.ArgumentParser("Factorizes a number using a SAT solver")
    parser.add_argument("number_to_factorize", type=int, help="the number to factorize")
    parser.add_argument("--solver", "-s", type=str, help="the solver to use", default=DEFAULT_SOLVER)
    parser.add_argument("--rho", "-r", action="store_const", default=False, const=True,
                        help="use Pollard's Rho-algorithm instead of a SAT solver")
    parser.add_argument("--naive", "-n", action="store_const", default=False, const=True,
                        help="use naive factorization instead of a SAT solver. Ignored if --rho is also used.")
    parser.add_argument("--non-recursive", action="store_const", default=False, const=True,
                        help="stop after finding a single factor instead of recursively factorizing the factors")
    parser.add_argument("-k", dest="karatsuba_threshold", type=int,
                        help="the number of bit the smaller factor must exceed for the Karatsuba algorithm to be used")
    parser.add_argument("-b", dest="multiplication_base", type=str,
                        choices=list(map(lambda entry: entry.name, FactToSat.CircuitBuilder.MultiplicationBase)),
                        help="the multiplication method to be used if the inputs are too small for the Karatsuba"
                             " algorithm")

    # parsing arguments
    args: argparse.Namespace = parser.parse_args()

    # since asking the user for the enum value would require the rather unwieldy qualification, a small translation step
    # is used instead
    multiplication_base: FactToSat.CircuitBuilder.MultiplicationBase = FactToSat.CircuitBuilder.MultiplicationBase[
        args.multiplication_base] if args.multiplication_base else None

    # factorizing
    factors: list[int]
    if args.rho:
        factors = Primes.rho_factorize(args.number_to_factorize, not args.non_recursive)
    elif args.naive:
        factors = Primes.naive_factorize(args.number_to_factorize, not args.non_recursive)
    else:
        factors = sat_factorize(args.number_to_factorize, not args.non_recursive, args.solver, args.karatsuba_threshold,
                                multiplication_base)

    # turning the factor list into a pretty output
    if len(factors) < 2:
        print(args.number_to_factorize, "is prime")
    else:
        # counting the factors, then sorting them (by the factor, not the frequency, since the first entry has higher
        # priority in the sorting
        factors_with_frequency: list[(int, int)] = sorted(Counter(factors).items())

        # the last construct connects each factor with its frequency using "^" (unless the frequnecy is 1), then
        # connects the factor-frequency pairs with " * "
        print(args.number_to_factorize, "=",
              " * ".join(map(lambda pair: str(pair[0]) + "^" + str(pair[1]) if pair[1] > 1 else str(pair[0]),
                             factors_with_frequency)))
