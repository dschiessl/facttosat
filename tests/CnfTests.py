import os
import unittest
import io
from scripts.Cnf import BooleanVariable, Literal, Cnf
from scripts.Circuit import InputGate, NotGate, AndGate, OrGate, Circuit

# os.path.dirname(__file__) returns absolute path to the directory of this script
TEST_RESOURCE_PATH: str = os.path.dirname(__file__) + "/../testResources/cnfTests/"


class CnfToDimacsTests(unittest.TestCase):
    def verify_dimacs_equality(self, to_test: str, correct_str: str) -> None:
        """
        Tests if the given DIMACS strings describe the same formula, even if they are ordered differently

        :param to_test: the string which should actually be tested. Only for this one additional tests to verify its
        well-formedness are performed
        :param correct_str: the string describing the correct_result. Is assumed to be well-formed
        """

        print(to_test, correct_str, sep="\n\n")

        to_test_lines: list[str] = to_test.split("\n")
        correct_str_lines: list[str] = correct_str.split("\n")
        self.assertEqual(len(to_test_lines), len(correct_str_lines))
        current_line_index: int = 0
        # testing exact equality for the header
        while current_line_index < len(correct_str_lines):
            self.assertEqual(to_test_lines[current_line_index], correct_str_lines[current_line_index])

            if correct_str_lines[current_line_index].startswith("p "):
                # header finished
                break
            else:
                current_line_index += 1

        # last increment was skipped
        current_line_index += 1

        # for the clauses, order is irrelevant, so we must remove it
        # first, we split into literals
        to_test_clauses: list[list[str]] = list(map(lambda line: line.split(" "), to_test_lines[current_line_index:]))
        correct_str_clauses: list[list[str]] = list(
            map(lambda line: line.split(" "), correct_str_lines[current_line_index:]))

        # while order is irrelevant for the actual literals, the 0 must always be at the end (except for the empty line
        # at the end)
        for clause in to_test_clauses[:-1]:
            self.assertEqual(clause[-1], "0")

        # eliminating order by sorting
        to_test_clauses = sorted(map(sorted, to_test_clauses))
        correct_str_clauses = sorted(map(sorted, correct_str_clauses))

        self.assertEqual(to_test_clauses, correct_str_clauses)

    def verify_dimacs_conversion(self, formula: Cnf, correct_output_file_name: str) -> None:
        """
        Converts the formula to DIMACS and checks if the output is correct

        :param formula: the formula to convert
        :param correct_output_file_name: the name of the file containing the correct output
        """

        # load the correct output
        with open(TEST_RESOURCE_PATH + correct_output_file_name, "r") as correct_output_file:
            correct_output: str = correct_output_file.read()

        # this is run twice to see if the conversion breaks anything
        for i in range(2):
            # convert the formula to DIMACS
            output_stream: io.StringIO = io.StringIO()
            formula.to_dimacs(output_stream)
            output_stream.seek(0)
            output: str = output_stream.read()

            # compare
            self.verify_dimacs_equality(output, correct_output)

    def vde_test_helper(self, file_name: str) -> None:
        """
        Loads strings from the specified files, then runs verify_dimacs_equality on them. Meant for supposed-to-fail
        tests, will not fail if files can't be opened

        :param file_name: the base file name. To get the file for the parameter to_test, a "_t" is appended and for the
        file for correct_str a "_c"
        """
        try:
            to_test_file: io.TextIOBase = open(TEST_RESOURCE_PATH + file_name + "_t")
            to_test: str = to_test_file.read()
            to_test_file.close()
            correct_file: io.TextIOBase = open(TEST_RESOURCE_PATH + file_name + "_c")
            correct_str: str = correct_file.read()
            correct_file.close()
        except:
            # any failure here is not the expected failure
            return

        self.verify_dimacs_equality(to_test, correct_str)

    # The following tests are to catch false positives of verify_dimacs_equality

    @unittest.expectedFailure
    def test_vde_missing_comment(self) -> None:
        self.vde_test_helper("vde_missing_comment")

    @unittest.expectedFailure
    def test_vde_changed_comment(self) -> None:
        self.vde_test_helper("vde_changed_comment")

    @unittest.expectedFailure
    def test_vde_too_much_comment(self) -> None:
        self.vde_test_helper("vde_too_much_comment")

    @unittest.expectedFailure
    def test_vde_comment_out_of_order(self) -> None:
        self.vde_test_helper("vde_comment_out_of_order")

    @unittest.expectedFailure
    def test_vde_changed_variable_count(self) -> None:
        self.vde_test_helper("vde_changed_variable_count")

    @unittest.expectedFailure
    def test_vde_changed_clause_count(self) -> None:
        self.vde_test_helper("vde_changed_clause_count")

    @unittest.expectedFailure
    def test_vde_changed_type(self) -> None:
        self.vde_test_helper("vde_changed_type")

    @unittest.expectedFailure
    def test_vde_missing_literal(self) -> None:
        self.vde_test_helper("vde_missing_literal")

    @unittest.expectedFailure
    def test_vde_added_literal(self) -> None:
        self.vde_test_helper("vde_added_literal")

    @unittest.expectedFailure
    def test_vde_changed_literal(self) -> None:
        self.vde_test_helper("vde_changed_literal")

    @unittest.expectedFailure
    def test_vde_changed_polarity(self) -> None:
        self.vde_test_helper("vde_changed_polarity")

    @unittest.expectedFailure
    def test_vde_missing_clause(self) -> None:
        self.vde_test_helper("vde_missing_clause")

    @unittest.expectedFailure
    def test_vde_added_clause(self) -> None:
        self.vde_test_helper("vde_added_clause")

    @unittest.expectedFailure
    def test_vde_missing_empty_clause_start(self) -> None:
        self.vde_test_helper("vde_missing_empty_clause_start")

    @unittest.expectedFailure
    def test_vde_missing_empty_clause_middle(self) -> None:
        self.vde_test_helper("vde_missing_empty_clause_middle")

    @unittest.expectedFailure
    def test_vde_missing_empty_clause_end(self) -> None:
        self.vde_test_helper("vde_missing_empty_clause_end")

    @unittest.expectedFailure
    def test_vde_missing_empty_clause_only_clause(self) -> None:
        self.vde_test_helper("vde_missing_empty_clause_only_clause")

    @unittest.expectedFailure
    def test_vde_added_empty_clause_start(self) -> None:
        self.vde_test_helper("vde_added_empty_clause_start")

    @unittest.expectedFailure
    def test_vde_added_empty_clause_middle(self) -> None:
        self.vde_test_helper("vde_added_empty_clause_middle")

    @unittest.expectedFailure
    def test_vde_added_empty_clause_end(self) -> None:
        self.vde_test_helper("vde_added_empty_clause_end")

    @unittest.expectedFailure
    def test_vde_added_empty_clause_only_clause(self) -> None:
        self.vde_test_helper("vde_added_empty_clause_only_clause")

    @unittest.expectedFailure
    def test_vde_leading_space(self) -> None:
        self.vde_test_helper("vde_leading_space")

    @unittest.expectedFailure
    def test_vde_double_space(self) -> None:
        self.vde_test_helper("vde_double_space")

    @unittest.expectedFailure
    def test_vde_leading_space_empty_clause(self) -> None:
        self.vde_test_helper("vde_leading_space_empty_clause")

    @unittest.expectedFailure
    def test_vde_missing_zero(self) -> None:
        self.vde_test_helper("vde_missing_zero")

    @unittest.expectedFailure
    def test_vde_missing_zero_empty_clause(self) -> None:
        self.vde_test_helper("vde_missing_zero_empty_clause")

    @unittest.expectedFailure
    def test_vde_misplaced_zero(self) -> None:
        self.vde_test_helper("vde_misplaced_zero")

    @unittest.expectedFailure
    def test_vde_missing_empty_line(self) -> None:
        self.vde_test_helper("vde_missing_empty_line")

    @unittest.expectedFailure
    def test_vde_double_empty_line(self) -> None:
        self.vde_test_helper("vde_double_empty_line")

    @unittest.expectedFailure
    def test_vde_inner_empty_line(self) -> None:
        self.vde_test_helper("vde_inner_empty_line")

    @unittest.expectedFailure
    def test_vde_empty_line_before_clauses(self) -> None:
        self.vde_test_helper("vde_empty_line_before_clauses")

    @unittest.expectedFailure
    def test_vde_empty_line_before_header(self) -> None:
        self.vde_test_helper("vde_empty_line_before_header")

    @unittest.expectedFailure
    def test_vde_empty_line_within_comment(self) -> None:
        self.vde_test_helper("vde_empty_line_within_comment")

    @unittest.expectedFailure
    def test_vde_empty_line_before_comment(self) -> None:
        self.vde_test_helper("vde_empty_line_before_comment")

    # Now, the actual tests

    def test_single_unit_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable))

        self.verify_dimacs_conversion(formula, "single_unit_clause")

    def test_single_negative_unit_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable, False))

        self.verify_dimacs_conversion(formula, "single_negative_unit_clause")

    def test_two_contradictory_unit_clauses_pos_neg(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable))
        formula.add_clause(Literal(variable, False))

        self.verify_dimacs_conversion(formula, "two_contradictory_unit_clauses_pos_neg")

    def test_two_contradictory_unit_clauses_neg_pos(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable, False))
        formula.add_clause(Literal(variable))

        self.verify_dimacs_conversion(formula, "two_contradictory_unit_clauses_neg_pos")

    def test_multiple_unit_clauses_different_variables(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        # variables added out of order to test naming guarantee
        formula.add_clause(Literal(variable1, False))
        formula.add_clause(Literal(variable3))
        formula.add_clause(Literal(variable4, False))
        formula.add_clause(Literal(variable2, False))

        self.verify_dimacs_conversion(formula, "multiple_unit_clauses_different_variables")

    def test_multiple_unit_clauses_one_variable_repeated(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        # variables added out of order to test naming guarantee
        formula.add_clause(Literal(variable2, False))
        formula.add_clause(Literal(variable4, False))
        formula.add_clause(Literal(variable1))
        formula.add_clause(Literal(variable2))
        formula.add_clause(Literal(variable3))

        self.verify_dimacs_conversion(formula, "multiple_unit_clauses_one_variable_repeated")

    def test_multiple_unit_clauses_multiple_variables_repeated(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()

        # variables added out of order to test naming guarantee
        formula.add_clause(Literal(variable2))
        formula.add_clause(Literal(variable2, False))
        formula.add_clause(Literal(variable1, False))
        formula.add_clause(Literal(variable3, False))
        formula.add_clause(Literal(variable1))

        self.verify_dimacs_conversion(formula, "multiple_unit_clauses_multiple_variables_repeated")

    def test_single_clause_with_two_variables_different_polarity(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable2), Literal(variable1, False))

        self.verify_dimacs_conversion(formula, "single_clause_with_two_variables_different_polarity")

    def test_single_clause_with_two_variables_same_polarity(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1, False), Literal(variable2, False))

        self.verify_dimacs_conversion(formula, "single_clause_with_two_variables_same_polarity")

    def test_single_clause_with_multiple_variables(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()
        variable5: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1), Literal(variable2, False), Literal(variable5),
                           Literal(variable3, False), Literal(variable4))

        self.verify_dimacs_conversion(formula, "single_clause_with_multiple_variables")

    def test_square_cnf(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()
        variable5: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable2), Literal(variable3, False), Literal(variable4), Literal(variable5, False))
        formula.add_clause(Literal(variable1, False), Literal(variable3), Literal(variable4, False),
                           Literal(variable5, False))
        formula.add_clause(Literal(variable1), Literal(variable2, False), Literal(variable4), Literal(variable5, False))
        formula.add_clause(Literal(variable1, False), Literal(variable2), Literal(variable3, False),
                           Literal(variable5, False))

        self.verify_dimacs_conversion(formula, "square_cnf")

    def test_two_cnf(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable4, False), Literal(variable1))
        formula.add_clause(Literal(variable2, False), Literal(variable3, False))
        formula.add_clause(Literal(variable4), Literal(variable2))
        formula.add_clause(Literal(variable3, False), Literal(variable1))
        formula.add_clause(Literal(variable2, False), Literal(variable1))
        formula.add_clause(Literal(variable3), Literal(variable2))
        formula.add_clause(Literal(variable1), Literal(variable3))
        formula.add_clause(Literal(variable2, False), Literal(variable3))

        self.verify_dimacs_conversion(formula, "two_cnf")

    def test_different_clause_sizes(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable4, False), Literal(variable3, False), Literal(variable2, False),
                           Literal(variable1))
        formula.add_clause(Literal(variable2), Literal(variable3, False))
        formula.add_clause(Literal(variable3))
        formula.add_clause(Literal(variable1, False), Literal(variable3, False))

        self.verify_dimacs_conversion(formula, "different_clause_sizes")

    def test_reused_literal(self) -> None:
        # to test if reusing the same literal instead of creating a new one each time causes problems

        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        neg2: Literal = Literal(variable2, False)
        pos4: Literal = Literal(variable4)

        formula.add_clause(neg2, Literal(variable4))
        formula.add_clause(pos4, neg2, Literal(variable1, False))
        formula.add_clause(Literal(variable2, False), Literal(variable3), Literal(variable1, False), pos4)
        formula.add_clause(pos4, Literal(variable2), Literal(variable1))

        self.verify_dimacs_conversion(formula, "reused_literal")

    def test_empty_cnf(self) -> None:
        # create the formula
        formula: Cnf = Cnf()

        self.verify_dimacs_conversion(formula, "empty_cnf")

    def test_empty_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()

        formula.add_clause()

        self.verify_dimacs_conversion(formula, "empty_clause")

    def test_duplicate_literal_reference(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()

        pos1: Literal = Literal(variable1)

        formula.add_clause(pos1, pos1)

        self.verify_dimacs_conversion(formula, "duplicate_literal_reference")

    def test_duplicate_literal_value(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1, False), Literal(variable2), Literal(variable1, False))

        self.verify_dimacs_conversion(formula, "duplicate_literal_value")

    def test_duplicate_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable2, False), Literal(variable1))
        formula.add_clause(Literal(variable1, False))
        formula.add_clause(Literal(variable2, False), Literal(variable1))

        self.verify_dimacs_conversion(formula, "duplicate_clause")

    def test_duplicate_empty_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()

        formula.add_clause()
        formula.add_clause()

        self.verify_dimacs_conversion(formula, "duplicate_empty_clause")

    def test_unused_variable(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        unused: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1), Literal(variable4, False))
        formula.add_clause(Literal(variable2), Literal(variable1), Literal(variable3))
        formula.add_clause(Literal(variable3, False), Literal(variable2, False))

        self.verify_dimacs_conversion(formula, "unused_variable")

    def test_taut_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1), Literal(variable1, False))

        self.verify_dimacs_conversion(formula, "taut_clause")

    def test_variable_only_used_in_taut_clause(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        unused: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable2), Literal(variable1, False))
        formula.add_clause(Literal(variable1), Literal(unused, False), Literal(unused))
        formula.add_clause(Literal(variable2, False))

        self.verify_dimacs_conversion(formula, "variable_only_used_in_taut_clause")

    def test_oneline_comment(self) -> None:
        # create the formula
        formula: Cnf = Cnf()

        formula.set_comment("This is a single comment line")

        self.verify_dimacs_conversion(formula, "oneline_comment")

    def test_multiline_comment(self) -> None:
        # create the formula
        formula: Cnf = Cnf()

        formula.set_comment("A comment line\nAnd another")

        self.verify_dimacs_conversion(formula, "multiline_comment")

    def test_comment_with_variable(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable))

        formula.set_comment("Comment references the variable {}", [variable])

        self.verify_dimacs_conversion(formula, "comment_with_variable")

    def test_comment_with_unused_variable(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable: BooleanVariable = formula.create_variable()

        formula.set_comment("Variable {} is not used", [variable])

        self.verify_dimacs_conversion(formula, "comment_with_unused_variable")

    def test_comment_with_multiple_variables(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable1, False), Literal(variable3), Literal(variable2))

        formula.set_comment("Referencing both {}\nand {}", [variable3, variable1])

        self.verify_dimacs_conversion(formula, "comment_with_multiple_variables")

    def test_comment_with_reused_variable(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable2))
        formula.add_clause(Literal(variable3, False))
        formula.add_clause(Literal(variable1))

        formula.set_comment("variable {} appears here,\nthen we have variable {}, but {} appears again",
                            [variable2, variable3, variable2])

        self.verify_dimacs_conversion(formula, "comment_with_reused_variable")

    def test_comment_with_unused_and_reused_variables(self) -> None:
        # create the formula
        formula: Cnf = Cnf()
        variable1: BooleanVariable = formula.create_variable()
        unused1: BooleanVariable = formula.create_variable()
        variable2: BooleanVariable = formula.create_variable()
        variable3: BooleanVariable = formula.create_variable()
        variable4: BooleanVariable = formula.create_variable()
        unused2: BooleanVariable = formula.create_variable()

        formula.add_clause(Literal(variable3, False), Literal(variable1))
        formula.add_clause(Literal(variable2), Literal(variable4, False))

        formula.set_comment("We reference {}, {} and {}, even though {} is not used, and neither is {}\n"
                            "Also, here is {} again",
                            [variable3, variable1, unused1, unused1, unused2, variable3])

        self.verify_dimacs_conversion(formula, "comment_with_unused_and_reused_variables")

    def test_input_gate(self) -> None:
        circuit: Circuit = Circuit()

        circuit.add_gate(InputGate())

        self.verify_dimacs_conversion(circuit.to_cnf(), "input_gate")

    def test_not_gate(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        circuit.add_gate(NotGate(input1))

        self.verify_dimacs_conversion(circuit.to_cnf(), "not_gate")

    def test_or_gate_no_input(self) -> None:
        circuit: Circuit = Circuit()

        circuit.add_gate(OrGate())

        self.verify_dimacs_conversion(circuit.to_cnf(), "or_gate_no_input")

    def test_or_gate_one_input(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        circuit.add_gate(OrGate(input1))

        self.verify_dimacs_conversion(circuit.to_cnf(), "or_gate_one_input")

    def test_or_gate_two_inputs(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        input2 = circuit.add_gate(InputGate())
        circuit.add_gate(OrGate(input1, input2))

        self.verify_dimacs_conversion(circuit.to_cnf(), "or_gate_two_inputs")

    def test_or_gate_three_inputs(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        input2 = circuit.add_gate(InputGate())
        input3 = circuit.add_gate(InputGate())
        circuit.add_gate(OrGate(input1, input2, input3))

        self.verify_dimacs_conversion(circuit.to_cnf(), "or_gate_three_inputs")

    def test_and_gate_no_input(self) -> None:
        circuit: Circuit = Circuit()

        circuit.add_gate(AndGate())

        self.verify_dimacs_conversion(circuit.to_cnf(), "and_gate_no_input")

    def test_and_gate_one_input(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        circuit.add_gate(AndGate(input1))

        self.verify_dimacs_conversion(circuit.to_cnf(), "and_gate_one_input")

    def test_and_gate_two_inputs(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        input2 = circuit.add_gate(InputGate())
        circuit.add_gate(AndGate(input1, input2))

        self.verify_dimacs_conversion(circuit.to_cnf(), "and_gate_two_inputs")

    def test_and_gate_three_inputs(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        input2 = circuit.add_gate(InputGate())
        input3 = circuit.add_gate(InputGate())
        circuit.add_gate(AndGate(input1, input2, input3))

        self.verify_dimacs_conversion(circuit.to_cnf(), "and_gate_three_inputs")

    def test_constructed_xor(self) -> None:
        circuit: Circuit = Circuit()

        input1 = circuit.add_gate(InputGate())
        input2 = circuit.add_gate(InputGate())
        and1 = circuit.add_gate(AndGate(input1, input2))
        nand = circuit.add_gate(NotGate(and1))
        or1 = circuit.add_gate(OrGate(input1, input2))
        circuit.add_gate(AndGate(nand, or1))

        self.verify_dimacs_conversion(circuit.to_cnf(), "constructed_xor")
