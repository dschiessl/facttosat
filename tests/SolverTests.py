import os
import tempfile
import unittest
import subprocess
from functools import reduce
import math
import random

import Factorize
from scripts import SolverHandler, Util, Cnf, Primes, CircuitBuilder
from scripts.Circuit import Circuit, AndGate, NotGate, InputGate
from scripts.Cnf import Literal
from scripts.FactToSat import fact_to_sat

# os.path.dirname(__file__) returns absolute path to the directory of this script
SOLVER_PATH: str = os.path.dirname(__file__) + "/../solvers/kissat"
TEST_RESOURCE_PATH: str = os.path.dirname(__file__) + "/../testResources/solverTests/"

# since the normal Karatsuba threshold would prevent the Karatsuba algorithm from being used in the tests, we use a
# lower one instead
TEST_KARATSUBA_THRESHOLD: int = 9


class SolverTests(unittest.TestCase):
    def flip_bits(self, number: int, length: int) -> list[int]:
        """
        Applies every 0-, 1- and 2-bit flip to a number

        :param number: the number to change
        :param length: the number of bits within which the changes should be applied
        :return: a list of the resulting numbers
        """
        # 0-bit flip (nothing changed)
        result: list[int] = [number]

        # 1-bit flips
        for i in range(length):
            result.append(number ^ 1 << i)

        # # 2-bit flips # TODO: currently removed due to time requirements
        # for i in range(length):
        #     for j in range(i):
        #         result.append(number ^ 1 << i ^ 1 << j)

        return result

    def test_fact_primes(self) -> None:
        """
        Checks if fact_to_sat actually results in unsatisfiable formulas for prime numbers
        """

        PRIMES: list[int] = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]

        # parameters for auto generated primes
        PRIME_SIZES: list[int] = [10, 11, 12, 13, 14, 15, 21, 32]  # in bits
        PRIMES_PER_SIZE: int = 10

        # generate primes
        for size in PRIME_SIZES:
            for i in range(PRIMES_PER_SIZE):
                PRIMES.append(Primes.get_prime(size))

        # run the tests
        for prime in PRIMES:
            for base in CircuitBuilder.MultiplicationBase:
                with self.subTest(str(prime) + " " + base.name):
                    # converting
                    formula: Cnf = fact_to_sat(prime, karatsuba_threshold=TEST_KARATSUBA_THRESHOLD,
                                               multiplication_base=base)
                    with tempfile.TemporaryFile(mode="w+") as output_file:
                        formula.to_dimacs(output_file)

                        # calling solver
                        output_file.seek(0)
                        solver_result: subprocess.CompletedProcess = subprocess.run([SOLVER_PATH], stdin=output_file,
                                                                                    capture_output=True, text=True)

                    # checking result
                    self.assertEqual(SolverHandler.was_sat(solver_result.stdout), False,
                                     SolverHandler.get_satisfying_assignment(solver_result.stdout))

    def test_fact_composites(self) -> None:
        """
        Checks if the correct factors satisfy the result of fact_to_sat and (at least a sample of) the wrong ones don't
        """

        # each inner list contains the prime factors, which should be multiplied to get the tested composite number
        COMPOSITE_FACTORS: list[list[int]] = [[2, 2],
                                              [2, 2, 2],
                                              [2, 3],
                                              [2, 2, 3],
                                              [2, 3, 3],
                                              [3, 3],
                                              [3, 3, 3],
                                              [5, 7],
                                              [29, 31],
                                              [2, 31],
                                              [3, 11, 17],
                                              [5, 13, 17],
                                              [2, 3, 5, 7, 11, 13]]

        # parameters for auto generated factors
        FACTOR_SIZES: list[list[int]] = [[5, 6, 7],
                                         [3, 4, 5, 6, 7, 8],
                                         [2, 10],
                                         [3, 10],
                                         [2, 11],
                                         [3, 11],
                                         [2, 2, 10],
                                         [2, 3, 10],
                                         [3, 3, 10],
                                         [9, 10],
                                         [9, 11],
                                         [10, 10],
                                         [9, 10, 11],
                                         [10, 10, 10]]  # in bits
        FACTORS_PER_SIZE: int = 1

        # generate factors
        for size_set in FACTOR_SIZES:
            for i in range(FACTORS_PER_SIZE):
                COMPOSITE_FACTORS.append(list(map(Primes.get_prime, size_set)))

        # run the tests
        for factor_set in COMPOSITE_FACTORS:
            for base in CircuitBuilder.MultiplicationBase:
                with self.subTest(str(factor_set) + " " + base.name):
                    product: int = reduce(lambda x, y: x * y, factor_set)
                    print(product)

                    # testing every subset of factor_set, by using numbers as bitstrings indicating which entries to
                    # pick
                    for subset in range(2 ** len(factor_set)):
                        factor: int = 1
                        for i in range(len(factor_set)):
                            if subset & 1 << i:
                                factor *= factor_set[i]

                        # we're only interested in the smaller factor
                        if factor > 1 and factor ** 2 <= product:
                            other_factor: int = int(product / factor)

                            product_bits: list[bool] = Util.integer_to_binary_string(product)

                            # merging the factors for more convenient access
                            smaller_factor_length: int = math.ceil(len(product_bits) / 2)
                            larger_factor_length: int = len(product_bits) - 1
                            both_factors: int = (other_factor << smaller_factor_length) + factor

                            # we are not only checking the correct factors, but also slightly changed versions of them
                            altered_factors: list[int] = self.flip_bits(both_factors,
                                                                        smaller_factor_length + larger_factor_length)

                            for assignment in altered_factors:
                                formula = fact_to_sat(product, karatsuba_threshold=TEST_KARATSUBA_THRESHOLD,
                                                      multiplication_base=base)

                                # splitting the assignment and testing if it is a correct factorization
                                assignment_first_factor: int = assignment % (1 << smaller_factor_length)
                                assignment_second_factor: int = assignment >> smaller_factor_length
                                factorization_correct: bool = (
                                        assignment_first_factor * assignment_second_factor == product)

                                # forcing the assignment
                                # TODO: currently, this simply assumes that the first variables are the smaller factor,
                                #  followed by the larger one, each in ascending order of significance. While this will
                                #  probably remain true in the future, a better way to access the variables should be
                                #  implemented.
                                for i in range(smaller_factor_length + larger_factor_length):
                                    formula.add_clause(
                                        Literal(formula.registered_variables[i], bool(assignment & 1 << i)))

                                # converting
                                with tempfile.TemporaryFile(mode="w+") as output_file:
                                    formula.to_dimacs(output_file)

                                    # calling solver
                                    output_file.seek(0)
                                    solver_result: subprocess.CompletedProcess = subprocess.run(
                                        [SOLVER_PATH], stdin=output_file,
                                        capture_output=True, text=True)

                                # checking result
                                self.assertEqual(SolverHandler.was_sat(solver_result.stdout), factorization_correct,
                                                 " ".join([str(assignment_first_factor), str(assignment_second_factor),
                                                           str(product), str(assignment)]))

    def test_addition(self) -> None:
        # the size of the first summand, then the size of the second one, then the shift
        SUMMAND_SIZES_AND_SHIFT: list[(int, int, int)] = [(1, 1, 0),
                                                          (1, 1, 1),
                                                          (1, 2, 1),
                                                          (1, 3, 1),
                                                          (3, 1, 1),
                                                          (5, 5, 2),
                                                          (5, 6, 3),
                                                          (7, 4, 2),
                                                          (10, 8, 5),
                                                          (12, 12, 0),
                                                          (11, 13, 0),
                                                          (14, 13, 0)]

        TESTS_PER_CIRCUIT: int = 10

        for first_size, second_size, shift in SUMMAND_SIZES_AND_SHIFT:
            if shift < first_size:
                # size of the bigger input plus the carry bit
                output_size = max(first_size, second_size + shift) + 1
            else:
                # if there is no overlap, there is no carry bit
                output_size = second_size + shift

            for i in range(TESTS_PER_CIRCUIT):
                # testing the circuit with random inputs
                first_summand: int = random.getrandbits(first_size)
                second_summand_unshifted: int = random.getrandbits(second_size)
                second_summand_shifted: int = second_summand_unshifted << shift
                sum: int = first_summand + second_summand_shifted

                with self.subTest(" ".join([str(first_size), str(second_size), str(shift), str(first_summand),
                                            str(second_summand_unshifted)])):

                    # we are not only checking the correct sum, but also slightly changed versions of it
                    altered_sums: list[int] = self.flip_bits(sum, output_size)

                    for altered_sum in altered_sums:
                        sum_correct: bool = altered_sum == sum

                        circuit: Circuit = Circuit()

                        first_summand_inputs: list[int] = list(
                            map(lambda i: circuit.add_gate(InputGate()), range(first_size)))
                        second_summand_inputs: list[int] = list(
                            map(lambda i: circuit.add_gate(InputGate()), range(second_size)))

                        output_gates: list[int] = CircuitBuilder.add_addition(circuit, first_summand_inputs,
                                                                              second_summand_inputs, shift)

                        # checking if the number of output gates matches the expectations
                        self.assertEqual(len(output_gates), output_size)

                        # to compare to the sum, we add NOT gates to every bit which must be false, thereby creating a
                        # string for which each bit must be true. This can now simply be checked with an AND
                        for i in range(len(output_gates)):
                            if not altered_sum & 1 << i:
                                output_gates[i] = circuit.add_gate(NotGate(output_gates[i]))
                        circuit.add_gate(AndGate(*output_gates))

                        formula: Cnf = circuit.to_cnf()

                        # forcing the inputs to the chosen values
                        for i in range(first_size):
                            formula.add_clause(Literal(circuit.get_gate(first_summand_inputs[i]).get_variable(),
                                                       bool(first_summand & 1 << i)))
                        for i in range(second_size):
                            formula.add_clause(Literal(circuit.get_gate(second_summand_inputs[i]).get_variable(),
                                                       bool(second_summand_unshifted & 1 << i)))

                        # converting
                        with tempfile.TemporaryFile(mode="w+") as output_file:
                            formula.to_dimacs(output_file)

                            # calling solver
                            output_file.seek(0)
                            solver_result: subprocess.CompletedProcess = subprocess.run([SOLVER_PATH],
                                                                                        stdin=output_file,
                                                                                        capture_output=True, text=True)

                        # checking result
                        self.assertEqual(SolverHandler.was_sat(solver_result.stdout), sum_correct, altered_sum)

    def test_subtraction(self) -> None:
        # the size of the minuend and the subtrahend
        INPUT_SIZES: list[(int, int)] = [(0, 0),
                                         (0, 1),
                                         (1, 0),
                                         (1, 1),
                                         (0, 2),
                                         (2, 0),
                                         (1, 2),
                                         (2, 1),
                                         (2, 2),
                                         (1, 3),
                                         (3, 1),
                                         (2, 3),
                                         (3, 2),
                                         (3, 3),
                                         (10, 10),
                                         (11, 9),
                                         (9, 11),
                                         (11, 10),
                                         (10, 11),
                                         (11, 11),
                                         (9, 9),
                                         (5, 15),
                                         (15, 5),
                                         (5, 5)]

        TESTS_PER_CIRCUIT: int = 10

        for minuend_size, subtrahend_size in INPUT_SIZES:
            output_size: int
            if subtrahend_size == 0:
                # if nothing is subtracted, the minuend remains unchanged
                output_size = minuend_size
            else:
                # size of the bigger input plus the carry bit
                output_size = max(minuend_size, subtrahend_size) + 1

            for i in range(TESTS_PER_CIRCUIT):
                # testing the circuit with random inputs
                minuend: int = random.getrandbits(minuend_size)
                subtrahend: int = random.getrandbits(subtrahend_size)
                difference: int = minuend - subtrahend

                with self.subTest(" ".join([str(minuend_size), str(subtrahend_size), str(minuend), str(subtrahend)])):
                    # we are not only checking the correct difference, but also slightly changed versions of it
                    altered_differences: list[int] = self.flip_bits(difference, output_size)

                    for altered_difference in altered_differences:
                        difference_correct: bool = altered_difference == difference

                        circuit: Circuit = Circuit()

                        minuend_inputs: list[int] = list(
                            map(lambda i: circuit.add_gate(InputGate()), range(minuend_size)))
                        subtrahend_inputs: list[int] = list(
                            map(lambda i: circuit.add_gate(InputGate()), range(subtrahend_size)))

                        output_gates: list[int] = CircuitBuilder.add_subtraction(circuit, minuend_inputs,
                                                                                 subtrahend_inputs)

                        # checking if the number of output gates matches the expectations
                        self.assertEqual(len(output_gates), output_size)

                        # to compare to the difference, we add NOT gates to every bit which must be false, thereby
                        # creating a string for which each bit must be true. This can now simply be checked with an AND
                        for i in range(len(output_gates)):
                            if not altered_difference & 1 << i:
                                output_gates[i] = circuit.add_gate(NotGate(output_gates[i]))
                        circuit.add_gate(AndGate(*output_gates))

                        formula: Cnf = circuit.to_cnf()

                        # forcing the inputs to the chosen values
                        for i in range(minuend_size):
                            formula.add_clause(
                                Literal(circuit.get_gate(minuend_inputs[i]).get_variable(), bool(minuend & 1 << i)))
                        for i in range(subtrahend_size):
                            formula.add_clause(
                                Literal(circuit.get_gate(subtrahend_inputs[i]).get_variable(),
                                        bool(subtrahend & 1 << i)))

                        # converting
                        with tempfile.TemporaryFile(mode="w+") as output_file:
                            formula.to_dimacs(output_file)

                            # calling solver
                            output_file.seek(0)
                            solver_result: subprocess.CompletedProcess = subprocess.run([SOLVER_PATH],
                                                                                        stdin=output_file,
                                                                                        capture_output=True, text=True)

                        # checking result
                        self.assertEqual(SolverHandler.was_sat(solver_result.stdout), difference_correct,
                                         altered_difference)

    def test_sat_factorization(self) -> None:
        """
        Checks if sat_factorization works correctly
        """
        # each inner list contains the prime factors, which should be multiplied to get the tested composite number
        COMPOSITE_FACTORS: list[list[int]] = [[2, 2],
                                              [2, 2, 2],
                                              [2, 3],
                                              [2, 2, 3],
                                              [2, 3, 3],
                                              [3, 3],
                                              [3, 3, 3],
                                              [5, 7],
                                              [29, 31],
                                              [2, 31],
                                              [3, 11, 17],
                                              [5, 13, 17],
                                              [2, 3, 5, 7, 11, 13]]

        # parameters for auto generated factors
        FACTOR_SIZES: list[list[int]] = [[5, 6, 7],
                                         [3, 4, 5, 6, 7, 8],
                                         [2, 10],
                                         [3, 10],
                                         [2, 11],
                                         [3, 11],
                                         [2, 2, 10],
                                         [2, 3, 10],
                                         [3, 3, 10],
                                         [9, 10],
                                         [9, 11],
                                         [10, 10],
                                         [9, 10, 11],
                                         [10, 10, 10]]  # in bits
        FACTORS_PER_SIZE: int = 5

        # generate factors
        for size_set in FACTOR_SIZES:
            for i in range(FACTORS_PER_SIZE):
                COMPOSITE_FACTORS.append(list(map(Primes.get_prime, size_set)))

        # run the tests
        for factor_set in COMPOSITE_FACTORS:
            for base in CircuitBuilder.MultiplicationBase:
                with self.subTest(str(factor_set) + " " + base.name):
                    product: int = reduce(lambda x, y: x * y, factor_set)

                    recursive_factors: list[int] = Factorize.sat_factorize(product, True, SOLVER_PATH,
                                                                           karatsuba_threshold=TEST_KARATSUBA_THRESHOLD,
                                                                           multiplication_base=base)
                    non_recursive_factors: list[int] = Factorize.sat_factorize(product, False, SOLVER_PATH,
                                                                               karatsuba_threshold=TEST_KARATSUBA_THRESHOLD,
                                                                               multiplication_base=base)

                    # sorted to remove possible differences in order
                    self.assertEqual(sorted(recursive_factors), sorted(factor_set), recursive_factors)

                    # non recursive factorization should only return two factors, but they should be non-trivial and
                    # result in the correct product
                    self.assertEqual(len(non_recursive_factors), 2, non_recursive_factors)
                    self.assertGreaterEqual(min(non_recursive_factors), 2)
                    self.assertEqual(non_recursive_factors[0] * non_recursive_factors[1], product)

    def test_solver_handler_unsat(self) -> None:
        FORMULA_PATHS: list[str] = ["simple_unsat_formula", "unsat_formula_with_variables"]
        for formula in FORMULA_PATHS:
            with self.subTest(formula):
                # calling solver
                solver_result: subprocess.CompletedProcess = subprocess.run([SOLVER_PATH, TEST_RESOURCE_PATH + formula],
                                                                            capture_output=True, text=True)

                self.assertEqual(SolverHandler.was_sat(solver_result.stdout), False)

    def test_solver_handler_sat(self) -> None:
        FORMULA_PATHS_AND_ASSIGNMENTS: list[(str, dict)] = [("empty_formula", dict()),
                                                            ("one_positive_variable", {1: True}),
                                                            ("one_negative_variable", {1: False}),
                                                            ("two_variables", {1: False, 2: True}),
                                                            ("many_variables",
                                                             {1: True, 2: True, 3: True, 4: False, 5: False})]

        for formula, assignment in FORMULA_PATHS_AND_ASSIGNMENTS:
            with self.subTest(formula):
                # calling solver
                solver_result: subprocess.CompletedProcess = subprocess.run([SOLVER_PATH, TEST_RESOURCE_PATH + formula],
                                                                            capture_output=True, text=True)

                # checking if formula was recognized as SAT
                self.assertEqual(SolverHandler.was_sat(solver_result.stdout), True, solver_result)

                # checking if assignment was correctly decoded
                self.assertEqual(SolverHandler.get_satisfying_assignment(solver_result.stdout), assignment)
