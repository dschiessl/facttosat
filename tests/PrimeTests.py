import unittest
from functools import reduce

from scripts import Primes


class PrimeTests(unittest.TestCase):
    def test_mrt_primes(self) -> None:
        """
        Checks if mrt actually succeeds for primes
        """

        PRIMES: list[int] = [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 3313, 7621]

        for prime in PRIMES:
            with self.subTest(prime):
                self.assertEqual(Primes.mrt(prime), True)

    def test_mrt_composites(self) -> None:
        """
        Checks if mrt actually fails for composite numbers
        """

        # only odd inputs are allowed
        COMPOSITES: list[int] = [9, 15, 21, 25, 27, 33, 561, 1105, 1729, 141141, 356735]

        for composite in COMPOSITES:
            with self.subTest(composite):
                self.assertEqual(Primes.mrt(composite), False)

    def test_rho_factorization(self) -> None:
        """
        Checks if Rho factorization works correctly
        """
        # each inner list contains the prime factors, which should be multiplied to get the tested composite number
        COMPOSITE_FACTORS: list[list[int]] = [[2, 2],
                                              [2, 2, 2],
                                              [2, 3],
                                              [2, 2, 3],
                                              [2, 3, 3],
                                              [3, 3],
                                              [3, 3, 3],
                                              [5, 7],
                                              [29, 31],
                                              [2, 31],
                                              [3, 11, 17],
                                              [5, 13, 17],
                                              [2, 3, 5, 7, 11, 13]]

        # parameters for auto generated factors
        FACTOR_SIZES: list[list[int]] = [[5, 6, 7],
                                         [3, 4, 5, 6, 7, 8],
                                         [2, 10],
                                         [3, 10],
                                         [2, 11],
                                         [3, 11],
                                         [2, 2, 10],
                                         [2, 3, 10],
                                         [3, 3, 10],
                                         [9, 10],
                                         [9, 11],
                                         [10, 10],
                                         [9, 10, 11],
                                         [10, 10, 10]]  # in bits
        FACTORS_PER_SIZE: int = 10

        # generate factors
        for size_set in FACTOR_SIZES:
            for i in range(FACTORS_PER_SIZE):
                COMPOSITE_FACTORS.append(list(map(Primes.get_prime, size_set)))

        # run the tests
        for factor_set in COMPOSITE_FACTORS:
            with self.subTest(factor_set):
                product: int = reduce(lambda x, y: x * y, factor_set)

                recursive_factors: list[int] = Primes.rho_factorize(product, True)
                non_recursive_factors: list[int] = Primes.rho_factorize(product, False)

                # sorted to remove possible differences in order
                self.assertEqual(sorted(recursive_factors), sorted(factor_set), recursive_factors)

                # non recursive factorization should only return two factors, but they should be non-trivial and result
                # in the correct product
                self.assertEqual(len(non_recursive_factors), 2, non_recursive_factors)
                self.assertGreaterEqual(min(non_recursive_factors), 2)
                self.assertEqual(non_recursive_factors[0]*non_recursive_factors[1], product)

    def test_naive_factorization(self) -> None:
        """
        Checks if naive factorization works correctly
        """
        # each inner list contains the prime factors, which should be multiplied to get the tested composite number
        COMPOSITE_FACTORS: list[list[int]] = [[2, 2],
                                              [2, 2, 2],
                                              [2, 3],
                                              [2, 2, 3],
                                              [2, 3, 3],
                                              [3, 3],
                                              [3, 3, 3],
                                              [5, 7],
                                              [29, 31],
                                              [2, 31],
                                              [3, 11, 17],
                                              [5, 13, 17],
                                              [2, 3, 5, 7, 11, 13]]

        # parameters for auto generated factors
        FACTOR_SIZES: list[list[int]] = [[5, 6, 7],
                                         [3, 4, 5, 6, 7, 8],
                                         [2, 10],
                                         [3, 10],
                                         [2, 11],
                                         [3, 11],
                                         [2, 2, 10],
                                         [2, 3, 10],
                                         [3, 3, 10],
                                         [9, 10],
                                         [9, 11],
                                         [10, 10],
                                         [9, 10, 11],
                                         [10, 10, 10]]  # in bits
        FACTORS_PER_SIZE: int = 10

        # generate factors
        for size_set in FACTOR_SIZES:
            for i in range(FACTORS_PER_SIZE):
                COMPOSITE_FACTORS.append(list(map(Primes.get_prime, size_set)))

        # run the tests
        for factor_set in COMPOSITE_FACTORS:
            with self.subTest(factor_set):
                product: int = reduce(lambda x, y: x * y, factor_set)

                recursive_factors: list[int] = Primes.naive_factorize(product, True)
                non_recursive_factors: list[int] = Primes.naive_factorize(product, False)

                # sorted to remove possible differences in order
                self.assertEqual(sorted(recursive_factors), sorted(factor_set), recursive_factors)

                # non recursive factorization should only return two factors, but they should be non-trivial and result
                # in the correct product
                self.assertEqual(len(non_recursive_factors), 2)
                self.assertGreaterEqual(min(non_recursive_factors), 2)
                self.assertEqual(non_recursive_factors[0] * non_recursive_factors[1], product)

    def test_naive_factorization_primes(self) -> None:
        """
        Checks if mrt actually succeeds for primes
        """

        PRIMES: list[int] = [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 3313, 7621]

        for prime in PRIMES:
            with self.subTest(prime):
                recursive_factors = Primes.naive_factorize(prime, True)
                non_recursive_factors = Primes.naive_factorize(prime, False)

                # the only factor should be the prime itself
                self.assertEqual(len(recursive_factors), 1)
                self.assertEqual(recursive_factors[0], prime)
                self.assertEqual(len(non_recursive_factors), 1)
                self.assertEqual(non_recursive_factors[0], prime)
