import argparse
import random
import sys
from functools import reduce

from scripts import Primes, FactToSat

if __name__ == '__main__':
    # setting up parser
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="Generates a benchmark formula based on the factorization problem")
    parser.add_argument("factor_sizes", type=int, metavar="FACTOR_SIZE", nargs="*",
                        help="the sizes (in bits) of the prime factors of the number to factorize")
    parser.add_argument("--factors", "-f", type=int, metavar="FACTOR", nargs="*", default=list(),
                        help="allows to explicitly pass factors")
    parser.add_argument("--seed", "-s", type=int, help="the seed for the random number generator")
    parser.add_argument("-o", metavar="OUTPUT_FILE", dest="output_file", type=argparse.FileType("w"),
                        default=sys.stdout, help="the file to write the generated formula to")
    parser.add_argument("--output-number", "-n", action="store_const", default=False, const=True,
                        help="skips the generation of the formula and only outputs the number to factorize")
    parser.add_argument("-k", dest="karatsuba_threshold", type=int,
                        help="the number of bits the smaller factor must exceed for the Karatsuba algorithm to be used")
    parser.add_argument("-b", dest="multiplication_base", type=str,
                        choices=list(map(lambda entry: entry.name, FactToSat.CircuitBuilder.MultiplicationBase)),
                        help="the multiplication method to be used if the inputs are too small for the Karatsuba"
                             " algorithm")

    # parsing
    args: argparse.Namespace = parser.parse_args()

    # since asking the user for the enum value would require the rather unwieldy qualification, a small translation step
    # is used instead
    multiplication_base: FactToSat.CircuitBuilder.MultiplicationBase = FactToSat.CircuitBuilder.MultiplicationBase[
        args.multiplication_base] if args.multiplication_base else None

    # handling seed
    if args.seed:
        random.seed(args.seed)

    # generating number
    factors: list[int] = args.factors
    for factor_size in args.factor_sizes:
        factors.append(Primes.get_prime(factor_size))
    number_to_factor: int = reduce(lambda x, y: x * y, factors, 1)

    if args.output_number:
        # only outputs the number
        args.output_file.write(str(number_to_factor) + "\n")
    else:
        # creating formula
        FactToSat.fact_to_sat(number_to_factor, args.karatsuba_threshold, multiplication_base).to_dimacs(
            args.output_file)
