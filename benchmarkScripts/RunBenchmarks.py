import argparse
import json
import csv
import os
import subprocess
import sys
import timeit
from pathlib import Path

# this line is needed to allow imports from the sibling directory
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from scripts import SolverHandler, FactToSat, Util

# os.path.dirname(__file__) returns absolute path to the directory of this script
DEFAULT_SOLVER: str = os.path.dirname(os.path.dirname(__file__)) + "/solvers/kissat"

# names of subdirectories within the working directory
CONFIG_SUBDIR_NAME: str = "config"
RESULT_SUBDIR_NAME: str = "results"
FORMULA_SUBDIR_NAME: str = "formulas"

# the prefixes for the different kinds of result files
TIMES_PREFIX: str = "times"
FACTORS_PREFIX: str = "factors"

if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        "Runs the benchmarks after the numbers and the formulas have been created")
    parser.add_argument("directory", type=str, help="the path of the working directory")
    parser.add_argument("--solver", "-s", type=str, help="the solver to use", default=DEFAULT_SOLVER)

    args: argparse.Namespace = parser.parse_args()

    working_directory: Path = Path(args.directory)
    config_directory: Path = working_directory / CONFIG_SUBDIR_NAME
    result_directory: Path = working_directory / RESULT_SUBDIR_NAME
    formula_directory: Path = working_directory / FORMULA_SUBDIR_NAME

    # there might be multiple config files
    for config_file_path in config_directory.iterdir():
        config_data: dict
        with config_file_path.open("r") as config_file:
            config_data = json.load(config_file)

        for factor_size_set in config_data["factor_sizes"]:
            result_filename_base: str = "_".join(map(str, factor_size_set))

            # each size set and result type has its own results file
            times_path: Path = result_directory / (TIMES_PREFIX + result_filename_base)
            factors_path: Path = result_directory / (FACTORS_PREFIX + result_filename_base)

            # the numbers are in each type of result file, so we just pick one
            numbers: list[int]
            with times_path.open("r", newline="") as result_file:
                result_reader: csv.reader = csv.reader(result_file)

                # the numbers are in the first row, but the first entry is the header
                numbers = list(map(int, next(result_reader)[1:]))

            for multiplication_setting in config_data["multiplication_settings"]:
                header: str = multiplication_setting["base"] + "_" + str(multiplication_setting[
                                                                             "karatsuba_threshold"])

                # the rows to add to the result files
                times_row: list[str] = [header]
                factors_row: list[str] = [header]

                for number in numbers:
                    # the name of the formula contains the number and the multiplication settings
                    formula_path: Path = formula_directory / (str(number) + "_" + header)

                    # a status update, because the script will take very long
                    print(result_filename_base, formula_path)

                    # the actual benchmark
                    start_time: float = timeit.default_timer()
                    solver_result: subprocess.CompletedProcess = subprocess.run(
                        [args.solver, str(formula_path)], capture_output=True, text=True)
                    end_time: float = timeit.default_timer()

                    # checking if there was a mistake in the factorization
                    solver_result_correct: bool
                    if SolverHandler.was_sat(solver_result.stdout):
                        # locating the bits of the factors
                        first_factor_bits: list[int]
                        second_factor_bits: list[int]
                        with formula_path.open() as formula:
                            first_factor_bits, second_factor_bits = FactToSat.get_factor_variables(formula)

                        # extracting the factors
                        variable_values = SolverHandler.get_satisfying_assignment(solver_result.stdout)
                        first_factor: int = Util.binary_string_to_integer(
                            list(map(lambda var: variable_values[var], first_factor_bits)))
                        second_factor: int = Util.binary_string_to_integer(
                            list(map(lambda var: variable_values[var], second_factor_bits)))

                        # the product of the factors must actually equal the original number
                        solver_result_correct = first_factor * second_factor == number

                        # adds the first factor to the row
                        factors_row.append(str(first_factor))
                    else:
                        # the formula should only be unsat if the number was prime
                        solver_result_correct = len(factor_size_set) < 2

                        # since there are no actual factors, we just add the number itself
                        factors_row.append(str(number))

                    # adds the solving time to the row if the factorization was correct and marks the error
                    # otherwise
                    times_row.append(end_time - start_time if solver_result_correct else "error")

                # adds the rows to the appropriate files
                with times_path.open("a", newline="") as times_file:
                    times_writer: csv.writer = csv.writer(times_file)
                    times_writer.writerow(times_row)
                with factors_path.open("a", newline="") as factors_file:
                    factors_writer: csv.writer = csv.writer(factors_file)
                    factors_writer.writerow(factors_row)
