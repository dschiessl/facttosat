import argparse
import csv
import json
import os
import sys
from functools import reduce
from pathlib import Path

# this line is needed to allow imports from the sibling directory
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from scripts import Primes

# names of subdirectories within the working directory
CONFIG_SUBDIR_NAME: str = "config"
RESULT_SUBDIR_NAME: str = "results"

# the prefixes for the different kinds of result files
TIMES_PREFIX: str = "times"
FACTORS_PREFIX: str = "factors"
VARIABLES_PREFIX: str = "variables"
CLAUSES_PREFIX: str = "clauses"

# the name of the row with the numbers
NUMBER_HEADER_NAME: str = "numbers"

if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser("Generates the numbers to factorize for a mass benchmark")
    parser.add_argument("directory", type=str, help="the path of the working directory")

    args: argparse.Namespace = parser.parse_args()

    working_directory: Path = Path(args.directory)
    config_directory: Path = working_directory / CONFIG_SUBDIR_NAME
    result_directory: Path = working_directory / RESULT_SUBDIR_NAME

    # there might be multiple config files
    for config_file_path in config_directory.iterdir():
        with config_file_path.open("r") as config_file:
            config_data: dict = json.load(config_file)
            for factor_size_set in config_data["factor_sizes"]:
                # generating the actual numbers
                number_line: list[str] = [NUMBER_HEADER_NAME]
                for i in range(config_data["numbers_per_factor"]):
                    number_line.append(str(reduce(lambda x, y: x * y, map(Primes.get_prime, factor_size_set))))

                result_filename_base: str = "_".join(map(str, factor_size_set))

                # helper to write the number line in a results file
                def write_number_line(prefix: str):
                    result_path: Path = result_directory / (prefix + result_filename_base)
                    with result_path.open("w", newline="") as result_file:
                        result_writer: csv.writer = csv.writer(result_file)
                        result_writer.writerow(number_line)


                # each size set and result type gets its own results file
                write_number_line(TIMES_PREFIX)
                write_number_line(FACTORS_PREFIX)
                write_number_line(VARIABLES_PREFIX)
                write_number_line(CLAUSES_PREFIX)
