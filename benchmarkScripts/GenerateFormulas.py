import argparse
import json
import csv
import os
import sys
from pathlib import Path

# this line is needed to allow imports from the sibling directory
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from scripts import FactToSat
from scripts.Cnf import Cnf

# names of subdirectories within the working directory
CONFIG_SUBDIR_NAME: str = "config"
RESULT_SUBDIR_NAME: str = "results"
FORMULA_SUBDIR_NAME: str = "formulas"

# the prefixes for the different kinds of result files
VARIABLES_PREFIX: str = "variables"
CLAUSES_PREFIX: str = "clauses"

if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        "Generates the formulas after the numbers have been created")
    parser.add_argument("directory", type=str, help="the path of the working directory")

    args: argparse.Namespace = parser.parse_args()

    working_directory: Path = Path(args.directory)
    config_directory: Path = working_directory / CONFIG_SUBDIR_NAME
    result_directory: Path = working_directory / RESULT_SUBDIR_NAME
    formula_directory: Path = working_directory / FORMULA_SUBDIR_NAME

    # there might be multiple config files
    for config_file_path in config_directory.iterdir():
        with config_file_path.open("r") as config_file:
            config_data: dict = json.load(config_file)
            for factor_size_set in config_data["factor_sizes"]:
                result_filename_base: str = "_".join(map(str, factor_size_set))

                # each size set and result type has its own results file
                variables_path: Path = result_directory / (VARIABLES_PREFIX + result_filename_base)
                clauses_path: Path = result_directory / (CLAUSES_PREFIX + result_filename_base)

                # the numbers are in each type of result file, so we just pick one
                with variables_path.open("r", newline="") as result_file:
                    result_reader: csv.reader = csv.reader(result_file)

                    # the numbers are in the first row, but the first entry is the header
                    numbers: list[int] = list(map(int, next(result_reader)[1:]))

                    for multiplication_setting in config_data["multiplication_settings"]:
                        # the header for the result files and also part of the formula names
                        header: str = multiplication_setting["base"] + "_" + str(multiplication_setting[
                                                                                     "karatsuba_threshold"])

                        # the lines written into the result files
                        variables_line: list[str] = [header]
                        clauses_line: list[str] = [header]

                        # generating the formulas
                        for number in numbers:
                            # the name of the formula contains the number and the multiplication settings
                            formula_path: Path = formula_directory / (str(number) + "_" + header)

                            formula: Cnf = FactToSat.fact_to_sat(number, multiplication_setting["karatsuba_threshold"],
                                                                 FactToSat.CircuitBuilder.MultiplicationBase[
                                                                     multiplication_setting["base"]])

                            with formula_path.open("w+") as formula_file:
                                formula.to_dimacs(formula_file)

                                # reading the number of variables and clauses
                                formula_file.seek(0)
                                while True:
                                    # if we run out of lines, the file was incorrectly formatted
                                    formula_line: str = formula_file.readline()

                                    if formula_line.startswith("p "):
                                        # the formula's header was found, variable and clause numbers can be extracted
                                        line_parts: list[str] = formula_line.split(" ")

                                        # after the "p" and the "cnf", there is first the number of variables, then the
                                        # number of clauses in the header line
                                        variables_line.append(line_parts[2])
                                        clauses_line.append(line_parts[3].strip())

                                        # we don't need to read the rest of the file
                                        break

                        # writing the numbers of variables and clauses in their appropriate files
                        with variables_path.open("a", newline="") as variable_file:
                            variables_writer: csv.writer = csv.writer(variable_file)
                            variables_writer.writerow(variables_line)
                        with clauses_path.open("a", newline="") as clause_file:
                            clause_writer: csv.writer = csv.writer(clause_file)
                            clause_writer.writerow(clauses_line)
