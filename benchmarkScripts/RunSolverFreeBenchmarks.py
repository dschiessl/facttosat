import argparse
import json
import csv
import os
import sys
import timeit
from collections.abc import Callable
from pathlib import Path

# this line is needed to allow imports from the sibling directory
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from scripts import Primes

# names of subdirectories within the working directory
CONFIG_SUBDIR_NAME: str = "config"
RESULT_SUBDIR_NAME: str = "results"

# the prefixes for the different kinds of result files
TIMES_PREFIX: str = "times"
FACTORS_PREFIX: str = "factors"

# the factorization functions to run the benchmark for
FACTORIZATION_FUNCTIONS: list[Callable[[int, bool], list[int]]] = [Primes.naive_factorize, Primes.rho_factorize]

if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        "Runs the benchmarks with direct factorization algorithms after the numbers have been created")
    parser.add_argument("directory", type=str, help="the path of the working directory")

    args: argparse.Namespace = parser.parse_args()

    working_directory: Path = Path(args.directory)
    config_directory: Path = working_directory / CONFIG_SUBDIR_NAME
    result_directory: Path = working_directory / RESULT_SUBDIR_NAME

    # there might be multiple config files
    for config_file_path in config_directory.iterdir():
        config_data: dict
        with config_file_path.open("r") as config_file:
            config_data = json.load(config_file)

        for factor_size_set in config_data["factor_sizes"]:
            result_filename_base: str = "_".join(map(str, factor_size_set))

            # each size set and result type has its own results file
            times_path: Path = result_directory / (TIMES_PREFIX + result_filename_base)
            factors_path: Path = result_directory / (FACTORS_PREFIX + result_filename_base)

            # the numbers are in each type of result file, so we just pick one
            numbers: list[int]
            with times_path.open("r", newline="") as result_file:
                result_reader: csv.reader = csv.reader(result_file)

                # the numbers are in the first row, but the first entry is the header
                numbers = list(map(int, next(result_reader)[1:]))

            for factorization_function in FACTORIZATION_FUNCTIONS:
                header: str = factorization_function.__name__

                # the rows to add to the result files
                times_row: list[str] = [header]
                factors_row: list[str] = [header]

                for number in numbers:
                    # a status update, because the script might take a while
                    print(result_filename_base, header, number)

                    # the actual benchmark
                    start_time: float = timeit.default_timer()
                    # the factorization is run non-recursive
                    found_factors: list[int] = factorization_function(number, False)
                    end_time: float = timeit.default_timer()

                    # checking if there was a mistake in the factorization
                    solver_result_correct: bool
                    # checks if factors were found
                    if len(found_factors) == 2:
                        # the product of the factors must actually equal the original number
                        solver_result_correct = found_factors[0] * found_factors[1] == number

                        # adds the first factor to the row
                        factors_row.append(str(found_factors[0]))
                    else:
                        # if no factors where found, the number should be prime
                        solver_result_correct = len(factor_size_set) < 2

                        # since there are no actual factors, we just add the number itself
                        factors_row.append(str(number))

                    # adds the solving time to the row if the factorization was correct and marks the error
                    # otherwise
                    times_row.append(end_time - start_time if solver_result_correct else "error")

                # adds the rows to the appropriate files
                with times_path.open("a", newline="") as times_file:
                    times_writer: csv.writer = csv.writer(times_file)
                    times_writer.writerow(times_row)
                with factors_path.open("a", newline="") as factors_file:
                    factors_writer: csv.writer = csv.writer(factors_file)
                    factors_writer.writerow(factors_row)
