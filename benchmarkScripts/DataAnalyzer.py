import argparse
import csv
import re
from collections.abc import Callable
from pathlib import Path

import math
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.linear_model import LinearRegression
import numpy

# data for creating human-readable formats
translation_data: dict[str, dict[str, str]] = {
    "SCHOOL_29": {"name": "\"school method\" (Karatsuba if sufficient size)", "color": "lightblue", "marker": "o"},
    "SCHOOL_1000": {"name": "\"school method\" (without Karatsuba)", "color": "dodgerblue", "marker": "o"},
    "WALLACE_29": {"name": "Wallace trees (Karatsuba if sufficient size)", "color": "red", "marker": "o"},
    "WALLACE_1000": {"name": "Wallace trees (without Karatsuba)", "color": "darkred", "marker": "o"},
    "DADDA_29": {"name": "Dadda trees (Karatsuba if sufficient size)", "color": "sandybrown", "marker": "o"},
    "DADDA_1000": {"name": "Dadda trees (without Karatsuba)", "color": "chocolate", "marker": "o"},
    "rho_factorize": {"name": "Rho algorithm", "color": "blue", "marker": "o"},
    "naive_factorize": {"name": "naive algorithm", "color": "midnightblue", "marker": "o"}
}


def extract_data(directory: Path, factor_size_filter: Callable[[list[int]], bool] = lambda x: True) \
        -> dict[str, dict[str, dict[int, float]]]:
    """
    Extracts the benchmark results from all the files in the given directory

    :param directory: the directory to search for files
    :param factor_size_filter: a filter determining which factor sizes should be included
    :return: the extracted data, structured as follows: the outermost dictionary is keyed by the type of result (time
    taken, found factor, etc.), the next one by factorization method (usually the conversion parameters for the
    formulas) and the final one by the factorized number.
    """
    result: dict[str, dict[str, dict[int, float]]] = dict()

    for file_path in directory.iterdir():
        file_name: str = file_path.name

        # finds the index of the first occurrence of a number in the filename
        size_string_start: int = re.search(r"[0-9]", file_name).start()

        # the result ends when the first number starts
        result_type: str = file_name[:size_string_start]
        # the factor sizes start with the first  number
        factor_size_string: str = file_name[size_string_start:]

        # parsing factor sizes
        factor_sizes: list[int] = list(map(int, factor_size_string.split("_")))

        # creating the subdictionary for the result type if it does not already exist. Happens before filtering to avoid
        # KeyErrors when accessing a specific type on an empty result
        if result_type not in result:
            result[result_type] = dict()

        # skipping any files whose factor sizes do not match the filter
        if not factor_size_filter(factor_sizes):
            continue

        with file_path.open("r", newline="") as file:
            reader: csv.reader = csv.reader(file)

            # the first line contains the factorized numbers (though the fist entry is only the header)
            numbers: list[int] = list(map(int, next(reader)[1:]))

            for line in reader:
                # the header states the factorization method
                factorization_method: str = line[0]

                # casts every result to a float. Not pretty, but I don't know how to avoid it
                result_data: list[float] = list(map(float, line[1:]))

                # creating the subdictionary for the factorization method if it does not already exist
                if factorization_method not in result[result_type]:
                    result[result_type][factorization_method] = dict()

                # putting the results in the dictionary
                for i in range(len(numbers)):
                    result[result_type][factorization_method][numbers[i]] = result_data[i]

    return result


def filter_primes(factors: list[int]) -> bool:
    return len(factors) == 1


def filter_fifteen(factors: list[int]) -> bool:
    return len(factors) == 2 and min(factors) == 15


def filter_pure_fifteen(factors: list[int]) -> bool:
    return filter_fifteen(factors) and filter_large_numbers(factors)


def filter_even_split(factors: list[int]) -> bool:
    return len(factors) == 2 and factors[0] == factors[1]


def filter_uneven_split(factors: list[int]) -> bool:
    return len(factors) == 2 and min(factors) == max(factors) - 2


def filter_four_factors(factors: list[int]) -> bool:
    return len(factors) == 4


def filter_large_numbers(factors: list[int]) -> bool:
    return sum(factors) >= 60


def filter_small_numbers(factors: list[int]) -> bool:
    return not filter_large_numbers(factors)


if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        "Collects the results from the benchmarks and processes them in way specified in the script")
    parser.add_argument("directory", type=str, help="the path of the directory with the results")

    args: argparse.Namespace = parser.parse_args()

    directory: Path = Path(args.directory)

    results: dict[str, dict[str, dict[int, float]]] = extract_data(directory)

    for factorization_method, result_dict in results["times"].items():
        plt.scatter([results["factors"]["naive_factorize"][key] for key in result_dict.keys()], result_dict.values(),
                    s=12, c=translation_data[factorization_method]["color"],
                    marker=translation_data[factorization_method]["marker"],
                    label=translation_data[factorization_method]["name"])

    plt.loglog()
    plt.legend(bbox_to_anchor=(1, 1), loc="upper left")
    plt.xlabel("smallest factor")
    plt.ylabel("time in seconds")
    plt.savefig("times.png", bbox_inches="tight")
